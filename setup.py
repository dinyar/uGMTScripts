#!/usr/bin/env python3
import urllib
import argparse
import os
import subprocess

def parse_options():
    desc = "Setup script for uGMTScripts"
    parser = argparse.ArgumentParser(description=desc, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--firmware_path', dest='firmware_path', type=str, default='', help='Path to firmware sources.')
    opts = parser.parse_args()
    return opts


def get_ugmt_constants(path):
    subprocess.run(['cp', path+'/uGMT_algos/firmware/hdl/common/ugmt_constants.vhd', 'ugmt_patterns/data/ugmt_constants.vhd'])
    # urllib.urlretrieve ("https://raw.githubusercontent.com/dinyar/uGMTfirmware/master/uGMT_algos/firmware/hdl/common/ugmt_constants.vhd", 
    #     "ugmt_patterns/data/ugmt_constants.vhd")

def main():
    opts = parse_options()
    if opts.firmware_path != '':
        print('Fetching ugmt_constants.vhd')
        get_ugmt_constants(opts.firmware_path)


if __name__ == "__main__":
    main()
