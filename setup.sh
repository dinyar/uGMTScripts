pathadd() {
    if [ -d "$1" ] && [[ ":$PYTHONPATH:" != *":$1:"* ]]; then
        PYTHONPATH="${PYTHONPATH:+"$PYTHONPATH:"}$1"
        echo "- $PYTHONPATH += $1"
    fi
}

SCRIPTPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

pathadd "$SCRIPTPATH"
pathadd "$SCRIPTPATH/ugmt_patterns"

export PYTHONPATH
