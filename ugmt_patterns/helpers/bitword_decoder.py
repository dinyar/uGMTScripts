#!/bin/env python

import click
from tools.vhdl import VHDLConstantsParser
from muon import Muon

# @click.command()
# @click.argument('firstword', type=int, description="something")
# @click.argument('tbfile', type=int)
# @click.option('--logfile', default='compareEmuWithTestbench.log', type=click.File('w'))
# def main(emufile, tbfile, logfile):
#     mu = None


@click.command()
@click.argument('ugmtconstantsfile', type=str)
@click.argument('firstword', type=str)
@click.argument('secondword', type=str)
@click.option('--eta', type=int, default=None, help="Eta value. *IMPORTANT:* Needed if this is an output muon as eta isn't contained in the two primary words then.")
@click.option('--linkid', type=int, default=None, help="LinkID. *IMPORTANT:* If supplied we assume this is an input muon!")
def main(ugmtconstantsfile, firstword, secondword, eta, linkid):
    vhdl_dict = VHDLConstantsParser.parse_vhdl_file(ugmtconstantsfile)
    firstword = firstword.replace('1v', '0x')
    secondword = secondword.replace('1v', '0x')
    fullword = (int(secondword, 16)<<32) + int(firstword, 16)
    mu = None
    if linkid is not None:
        mu = Muon(vhdl_dict, "IN", fullword, link=linkid, frame=0, bx=0)
    elif eta is not None:
        mu = Muon(vhdl_dict, "OUT", fullword, link=0, frame=0, bx=0, eta_out = eta)
    else:
        print "Either linkid or eta need to be supplied!"
        returnpull

    print
    print "Bitword is:\t {}".format(hex(mu.bitword))
    print "LSW is:\t\t {}".format(hex(mu.getLSW()))
    print "MSW is:\t\t {}".format(hex(mu.getMSW()))
    print "pT is:\t\t {}".format(mu.ptBits)
    print "phi is:\t\t {}".format(mu.phiBits)
    print "Global phi is:\t {}".format(mu.globPhiBits)
    print "eta is:\t\t {}".format(mu.etaBits)
    print "Iso is:\t\t {}".format(mu.Iso)
    print "Sysign is:\t {}".format(mu.Sysign)
    print "Sysign is:\t {}".format(mu.Sysign)
    print "qualityBits is:\t {}".format(mu.qualityBits)
    if linkid is not None and linkid < 60 and linkid > 47:
        print "Track address is:"
        print "Detector side is:\t\t {}".format(mu.trackAddress[0])
        print "Wheel number is:\t\t {}".format(mu.trackAddress[1])
        print "Station 1 (4 for KMTF) is:\t {}".format(mu.trackAddress[2])
        print "Station 2 (3 for KMTF) is:\t {}".format(mu.trackAddress[3])
        print "Station 3 (2 for KMTF) is:\t {}".format(mu.trackAddress[4])
        print "Station 4 (1 for KMTF) is:\t {}".format(mu.trackAddress[5])


if __name__ == '__main__':
    main()
