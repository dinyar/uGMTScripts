from tools.bithelper import bithlp


class Muon():

    """
    A class capable of interpreting the uGMT emulator and hardware muon representations
    """

    def __init__(self, vhdl_dict, mu_type, bitword=None, obj=None, link=-1, frame=-1, bx=-1, gPhi=None, eta_out=None):
        """
        ctor:
        TAKES:
            vhdl_dict   as returned by ../../tools/vhdl.VHDLConstantsParser
            mu_type     either IN (inputs), OUT (outputs), SER (serializer), or IMD (intermediates)
            bitword     can be None or a 64bit integer (for HW muons)
            obj         can be None or one of the emulator objects (for emulator muons)
            link        integer representing link the muon was received / is sent (HW only)
            bx          integer indicating the bunch-crossing the muon is associated with
        """

        # get the bit boundaries for the muon quantities
        self.bx = bx

        self.frame = frame
        self.haloFine = 0
        self.etaFine = 0
        self.tftype = -1
        self.tfside = 0
        self.link = link
        self.local_link = link
        if self.link != -1:
            self.local_link = self.link - 36
            if self.local_link <= vhdl_dict["EMTF_POS_HIGH"]:
                self.tftype = 2
                self.tfside = 1
            elif self.local_link <= vhdl_dict["OMTF_POS_HIGH"]:
                self.local_link -= vhdl_dict["OMTF_POS_LOW"]
                self.tftype = 1
                self.tfside = 1
            elif self.local_link <= vhdl_dict["BMTF_HIGH"]:
                self.local_link -= vhdl_dict["BMTF_LOW"]
                self.tftype = 0
                self.tfside = 0
            elif self.local_link <= vhdl_dict["OMTF_NEG_HIGH"]:
                self.local_link -= vhdl_dict["OMTF_NEG_LOW"]
                self.tftype = 1
                self.tfside = -1
            elif self.local_link <= vhdl_dict["EMTF_NEG_HIGH"]:
                self.local_link -= vhdl_dict["EMTF_NEG_LOW"]
                self.tftype = 2
                self.tfside = -1

        if mu_type == 'IMD' or mu_type == 'OUT' or mu_type == 'SER' or mu_type == 'SCOUT':
            bitword_type = 'OUT'
        else:
            bitword_type = 'IN'

        pt_low = vhdl_dict["PT_{t}_LOW".format(t=bitword_type)]
        pt_high = vhdl_dict["PT_{t}_HIGH".format(t=bitword_type)]

        sysign_low = vhdl_dict["SIGN_{t}".format(t=bitword_type)]
        sysign_high = vhdl_dict["VALIDSIGN_{t}".format(t=bitword_type)]

        trackadd_low = 0
        trackadd_high = 0

        qual_low = vhdl_dict["QUAL_{t}_LOW".format(t=bitword_type)]
        qual_high = vhdl_dict["QUAL_{t}_HIGH".format(t=bitword_type)]

        phi_low = vhdl_dict["PHI_{t}_LOW".format(t=bitword_type)]
        phi_high = vhdl_dict["PHI_{t}_HIGH".format(t=bitword_type)]

        if bitword_type == "OUT":
            iso_low = vhdl_dict["ISO_OUT_LOW"]
            iso_high = vhdl_dict["ISO_OUT_HIGH"]
            idx_low = vhdl_dict["IDX_OUT_LOW"]
            idx_high = vhdl_dict["IDX_OUT_HIGH"]
            impact_param_low = vhdl_dict["DISPL_IMPACT_PARAM_OUT_LOW"]
            impact_param_high = vhdl_dict["DISPL_IMPACT_PARAM_OUT_HIGH"]
            if mu_type == 'IMD':
                pt_unconstr_low = vhdl_dict["DISPL_PT_INT_LOW"]
                pt_unconstr_high = vhdl_dict["DISPL_PT_INT_HIGH"]
                eta_low = vhdl_dict["ETA_INT_LOW"]
                eta_high = vhdl_dict["ETA_INT_HIGH"]
            else:
                phi_extrapolated_low = vhdl_dict["PHI_EXTRAPOLATED_LOW"]
                phi_extrapolated_high = vhdl_dict["PHI_EXTRAPOLATED_HIGH"]
                eta_extrapolated_low = vhdl_dict["ETA_EXTRAPOLATED_LOW"]
                eta_extrapolated_high = vhdl_dict["ETA_EXTRAPOLATED_HIGH"]
                pt_unconstr_low = vhdl_dict["DISPL_PT_OUT_LOW"]
                pt_unconstr_high = vhdl_dict["DISPL_PT_OUT_HIGH"]
        elif bitword_type == "IN":
            eta_low = vhdl_dict["ETA_IN_LOW"]
            eta_high = vhdl_dict["ETA_IN_HIGH"]
            trackadd_low = vhdl_dict["BMTF_ADDRESS_STATION_1_IN_LOW"] - 2
            trackadd_high = vhdl_dict["BMTF_DETECTOR_SIDE_HIGH"] + 4
            hf_low = vhdl_dict["HALO_FINE_IN"]
            # Offset needed because fw sees words as 31 bit long.
            sysign_low += 1
            sysign_high += 1
            trackadd_low += 1
            trackadd_high += 1
            self.trackAddress = [0]*6
            if self.tftype == 0:
                # Offset needed because fw sees words as 31 bit long.
                pt_unconstr_low = vhdl_dict["BMTF_DISPL_PT_IN_LOW"] + 1
                pt_unconstr_high = vhdl_dict["BMTF_DISPL_PT_IN_HIGH"] + 1
                impact_param_low = vhdl_dict["BMTF_DISPL_IMPACT_PARAM_IN_LOW"] + 1
                impact_param_high = vhdl_dict["BMTF_DISPL_IMPACT_PARAM_IN_HIGH"] + 1
            elif self.tftype == 1:
                # Offset needed because fw sees words as 31 bit long.
                pt_unconstr_low = vhdl_dict["OMTF_DISPL_PT_IN_LOW"] + 1
                pt_unconstr_high = vhdl_dict["OMTF_DISPL_PT_IN_HIGH"] + 1
                impact_param_low = 0
                impact_param_high = 0
            elif self.tftype == 2:
                # Offset needed because fw sees words as 31 bit long.
                pt_unconstr_low = vhdl_dict["EMTF_DISPL_PT_IN_LOW"] + 1
                pt_unconstr_high = vhdl_dict["EMTF_DISPL_PT_IN_HIGH"] + 1
                impact_param_low = vhdl_dict["EMTF_DISPL_IMPACT_PARAM_IN_LOW"] + 1
                impact_param_high = vhdl_dict["EMTF_DISPL_IMPACT_PARAM_IN_HIGH"] + 1
            else:
                pt_unconstr_low = 0
                pt_unconstr_high = 0
                impact_param_low = 0
                impact_param_high = 0

        if obj == None and bitword != None:     # for hardware
            self.bitword = bitword
            self.Sysign = bithlp.get_shifted_subword(
                self.bitword, sysign_low, sysign_high)
            if mu_type == "IN" or mu_type == "IMD":
                self.etaBits = bithlp.get_shifted_subword(
                    self.bitword, eta_low, eta_high)
                self.etaBits = bithlp.twos_complement_to_signed(
                    self.etaBits, eta_high-eta_low+1)
            else:
                self.etaBits = eta_out
            self.qualityBits = bithlp.get_shifted_subword(
                self.bitword, qual_low, qual_high)
            self.ptBits = bithlp.get_shifted_subword(
                self.bitword, pt_low, pt_high)
            self.phiBits = bithlp.get_shifted_subword(
                self.bitword, phi_low, phi_high)
            if mu_type == "IN":
                self.phiBits = bithlp.twos_complement_to_signed(
                    self.phiBits, phi_high-phi_low+1)
            self.ptUnconstrBits = bithlp.get_shifted_subword(
                self.bitword, pt_unconstr_low, pt_unconstr_high)
            self.impactParam = bithlp.get_shifted_subword(
                self.bitword, impact_param_low, impact_param_high)
            self.globPhiBits = self.phiBits
            if bitword_type == "OUT":
                if self.ptBits > 0:
                    self.Iso = 3 # Everything is always isolated, because we don't have calo inputs 
                else:
                    self.Iso = 0
                if mu_type == 'SCOUT' or mu_type == "IMD":
                    self.Iso = 0
                if mu_type == "IMD":
                    self.phi_extrapol = -1
                    self.eta_extrapol = -9999
                    self.tfMuonIndex = -1
                else:
                    self.phi_extrapol = bithlp.get_shifted_subword(
                        self.bitword, phi_extrapolated_low, phi_extrapolated_high)
                    self.eta_extrapol = bithlp.get_shifted_subword(
                        self.bitword, eta_extrapolated_low, eta_extrapolated_high)
                    self.tfMuonIndex = bithlp.get_shifted_subword(
                        self.bitword, idx_low, idx_high)
                    if self.tfMuonIndex < 3*(vhdl_dict["EMTF_POS_HIGH"]+1):
                        self.tftype = 2
                        self.tfside = 1
                    elif self.tfMuonIndex < 3*(vhdl_dict["OMTF_POS_HIGH"]+1):
                        self.tftype = 1
                        self.tfside = 1
                    elif self.tfMuonIndex < 3*(vhdl_dict["BMTF_HIGH"]+1):
                        self.tftype = 0
                        self.tfside = 0
                    elif self.tfMuonIndex < 3*(vhdl_dict["OMTF_NEG_HIGH"]+1):
                        self.tftype = 1
                        self.tfside = -1
                    else:
                        self.tftype = 2
                        self.tfside = -1
            else:
                self.phi_extrapol = -1
                self.eta_extrapol = -9999
                self.Iso = 0
                self.tfMuonIndex = -1
                if self.local_link != -1:
                    self.globPhiBits = self.calcGlobalPhi(
                        self.phiBits, self.tftype, self.local_link)
                self.haloFine = bithlp.get_shifted_subword(
                    self.bitword, hf_low, hf_low+1)
                if self.tftype == 2:
                    self.etaFine = 1
                else:
                    self.etaFine = self.haloFine
                if self.tftype == 0:
                    # shift by +1 necessary because of the control bit 31
                    self.trackAddress[0] = bithlp.get_shifted_subword(
                        self.bitword, vhdl_dict["BMTF_DETECTOR_SIDE_LOW"] + 1, vhdl_dict["BMTF_DETECTOR_SIDE_HIGH"] + 1)
                    self.trackAddress[1] = bithlp.get_shifted_subword(
                        self.bitword, vhdl_dict["BMTF_WHEEL_NO_IN_LOW"] + 1, vhdl_dict["BMTF_WHEEL_NO_IN_HIGH"] + 1)
                    self.trackAddress[2] = bithlp.get_shifted_subword(
                        self.bitword, vhdl_dict["BMTF_ADDRESS_STATION_1_IN_LOW"] + 1, vhdl_dict["BMTF_ADDRESS_STATION_1_IN_HIGH"] + 1)
                    self.trackAddress[3] = bithlp.get_shifted_subword(
                        self.bitword, vhdl_dict["BMTF_ADDRESS_STATION_2_IN_LOW"] + 1, vhdl_dict["BMTF_ADDRESS_STATION_2_IN_HIGH"] + 1)
                    self.trackAddress[4] = bithlp.get_shifted_subword(
                        self.bitword, vhdl_dict["BMTF_ADDRESS_STATION_3_IN_LOW"] + 1, vhdl_dict["BMTF_ADDRESS_STATION_3_IN_HIGH"] + 1)
                    self.trackAddress[5] = bithlp.get_shifted_subword(
                        self.bitword, vhdl_dict["BMTF_ADDRESS_STATION_4_IN_LOW"] + 1, vhdl_dict["BMTF_ADDRESS_STATION_4_IN_HIGH"] + 1)
                elif self.tftype == 1:
                    self.trackAddress[0] = bithlp.get_shifted_subword(
                        self.bitword, trackadd_low, trackadd_high)
                elif self.tftype == 2:
                    self.trackAddress[0] = bithlp.get_shifted_subword(
                        self.bitword, trackadd_low, trackadd_high)

            self.rank = 0

        elif bitword == None and obj != None:  # for emulator
            if gPhi is not None:
                self.globPhiBits = gPhi
            else:
                self.globPhiBits = obj.hwPhi()
            if bitword_type == "OUT":
                if mu_type == "OUT" or mu_type == 'SER' or mu_type == 'SCOUT':
                    self.phi_extrapol = obj.hwPhiAtVtx()
                    self.eta_extrapol = obj.hwEtaAtVtx()
                else:
                    self.phi_extrapol = 0
                    self.eta_extrapol = 0
                if mu_type == 'SCOUT' or mu_type == "IMD":
                    self.Iso = 0
                else:
                    if obj.hwPt() > 0:
                        self.Iso = 3 # Always isolated, because we don't have calo inputs
                    else:
                        self.Iso = 0 
                self.rank = obj.hwRank()
                self.Sysign = obj.hwCharge() + (obj.hwChargeValid() << 1)
                self.tfMuonIndex = obj.tfMuonIndex()
            else:
                self.phi_extrapol = -1
                self.eta_extrapol = -9999
                if obj.hwPt() > 0:
                    self.Iso = 3 # Always isolated, because we don't have calo inputs
                else:
                    self.Iso = 0
                self.tfMuonIndex = -1
                self.rank = 0
                self.Sysign = obj.hwSign() + (obj.hwSignValid() << 1)
                self.trackAddress = obj.trackAddress()
                self.haloFine = obj.hwHF()
                self.etaFine = self.haloFine
                self.tftype = obj.trackFinderType()
                if self.tftype == 1 or self.tftype == 2:
                    self.tftype = 1
                elif self.tftype == 3 or self.tftype == 4:
                    self.tftype = 2
                    self.etaFine = 1
                if gPhi is None:
                    self.globPhiBits = self.calcGlobalPhi(
                        obj.hwPhi(), obj.trackFinderType(), obj.processor())

            self.phiBits = obj.hwPhi()
            unsigned_phi = bithlp.twos_complement_to_unsigned(
                obj.hwPhi(), phi_high-phi_low+1)
            self.etaBits = obj.hwEta()
            if mu_type == "IMD" or mu_type == "IN":
                unsigned_eta = bithlp.twos_complement_to_unsigned(
                    obj.hwEta(), eta_high-eta_low+1)
            self.qualityBits = obj.hwQual()
            self.ptBits = obj.hwPt()
            self.ptUnconstrBits = obj.hwPtUnconstrained()
            self.impactParam = obj.hwDXY()

            # calculate the bitword to make comparison with HW easy
            self.bitword = (self.ptBits << pt_low)
            self.bitword += (self.qualityBits << qual_low)
            self.bitword += (self.Sysign << sysign_low)
            if mu_type == "IMD" or mu_type == "IN":
                self.bitword += (unsigned_eta << eta_low)
            self.bitword += (unsigned_phi << phi_low)
            if bitword_type == "OUT":
                self.bitword += (self.ptUnconstrBits << pt_unconstr_low)
                self.bitword += (self.impactParam << impact_param_low)

            if bitword_type == "OUT" and self.Iso > 0:
                self.bitword += (self.Iso << iso_low)
            if mu_type == "OUT" or mu_type == "SER" or mu_type == 'SCOUT':
                if self.phi_extrapol >= 0:
                    self.bitword += (self.phi_extrapol << phi_extrapolated_low)
                if self.eta_extrapol >= -1 * (1 << (eta_extrapolated_high - eta_extrapolated_low)):
                    unsigned_eta_extrapol = bithlp.twos_complement_to_unsigned(
                        self.eta_extrapol, eta_extrapolated_high - eta_extrapolated_low + 1)
                    self.bitword += (unsigned_eta_extrapol <<
                                     eta_extrapolated_low)
                if self.tfMuonIndex >= 0:
                    self.bitword += (self.tfMuonIndex << idx_low)
            if bitword_type != "OUT":
                self.bitword += (self.haloFine << hf_low)

            if self.tftype == 0:
                # shift by +1 necessary because of the control bit 31
                self.bitword += self.trackAddress[0] << vhdl_dict["BMTF_DETECTOR_SIDE_LOW"] + 1
                self.bitword += self.trackAddress[1] << vhdl_dict["BMTF_WHEEL_NO_IN_LOW"] + 1
                self.bitword += self.trackAddress[2] << vhdl_dict["BMTF_ADDRESS_STATION_1_IN_LOW"] + 1
                self.bitword += self.trackAddress[3] << vhdl_dict["BMTF_ADDRESS_STATION_2_IN_LOW"] + 1
                self.bitword += self.trackAddress[4] << vhdl_dict["BMTF_ADDRESS_STATION_3_IN_LOW"] + 1
                self.bitword += self.trackAddress[5] << vhdl_dict["BMTF_ADDRESS_STATION_4_IN_LOW"] + 1
                self.bitword += (self.ptUnconstrBits <<
                                 vhdl_dict["BMTF_DISPL_PT_IN_LOW"] + 1)
                self.bitword += (self.impactParam <<
                                 vhdl_dict["BMTF_DISPL_IMPACT_PARAM_IN_LOW"] + 1)
            elif self.tftype == 1:
                self.bitword += self.trackAddress[0] << trackadd_low
                self.bitword += (self.ptUnconstrBits <<
                                 vhdl_dict["OMTF_DISPL_PT_IN_LOW"] + 1)
            elif self.tftype == 2:
                self.bitword += self.trackAddress[0] << trackadd_low
                self.bitword += (self.ptUnconstrBits <<
                                 vhdl_dict["EMTF_DISPL_PT_IN_LOW"] + 1)
                self.bitword += (self.impactParam <<
                                 vhdl_dict["EMTF_DISPL_IMPACT_PARAM_IN_LOW"] + 1)

        if self.etaBits == None:
            print('{mtype} {tftype} bitword  {word:0>16x}, {bit:x}'.format(mtype=mu_type, tftype=self.tftype, word=self.bitword, bit=self.qualityBits))
            print("Eta at end: " + str(self.etaBits))
            print("phi at end: " + str(self.phiBits))
            print()
            raise ValueError('Eta not set!')

    def setBunchCounter(self, n_mu):
        if n_mu == 1:
            self.bitword += ((self.bx & 0b1) << 31)
            # since it is the second bit only need to shift by 62 instead of 63
            self.bitword += (self.bx & 0b10) << 62
        if n_mu == 2:
            self.bitword += (((self.bx & 0b100) >> 2) << 31)
        if self.bx == 0 and n_mu == 0:
            self.bitword += 1 << 31
        pass

    def getBx(self):
        """
        returns the assiciated bunch-crossing
        """
        return self.bx

    def getRank(self):
        return self.rank

    def getLSW(self):
        mask = 0xffffffff
        return (self.bitword & mask)

    def getMSW(self):
        return (self.bitword >> 32)

    def encode_phi(self, phi):
        """
        As the hardware expects a control bit on position 31 and phi
        goes across the word boundary we need to put a zero in the
        middle.
        """
        mask_lsw = bithlp.get_mask(0, 5)  # 0-5 for phi
        lsw = int(phi) & mask_lsw
        msw = int(phi) >> 6
        encoded_phi = lsw + (msw << 7)
        return encoded_phi

    def decode_phi(self, xlow, xup):
        """
        for the HW represntation: the phi variable goes across 32bit word boundary
        at the boundary a control bit is reserved
        """
        # this is a specialized function because phi reaches over the word boundary
        ctrl_mask = bithlp.get_mask(31, 31)
        # +1 because dinyar shaves off the 32nd bit in the vhdl-file
        raw_mask = bithlp.get_mask(xlow, xup+1)
        # mask is 11110111111 for phi
        mask = raw_mask ^ ctrl_mask
        raw_val = self.bitword & mask
        raw_val = raw_val >> xlow
        lsw_mask = bithlp.get_mask(0, 5)
        msw_mask = bithlp.get_mask(7, 10)
        val = (raw_val & lsw_mask) + ((raw_val & msw_mask) >> 1)
        return val

    def calcGlobalPhi(self, locPhi, tftype, processor):
        """
        Calculate the global phi from the local phi of the TF candidate, the TF type, and the processor number
        """
        globPhi = 0
        if tftype == 0:  # BMTF
            # each BMTF processor corresponds to a 30 degree wedge = 48 in int-scale
            globPhi = processor * 48 + locPhi
            # first processor starts at CMS phi = -15 degrees...
            globPhi += 576 - 24
            # handle wrap-around (since we add the 576-24, the value will never be negative!)
            globPhi %= 576
        else:
            # all others correspond to 60 degree sectors = 96 in int-scale
            globPhi = processor * 96 + locPhi
            # first processor starts at CMS phi = 15 degrees... Handle wrap-around with %: Add 576 to make sure the number is positive
            globPhi = (globPhi + 600) % 576
        return globPhi

    @staticmethod
    def get_eta_from_outputword(vhdl_dict, outputword):
        eta1 = bithlp.get_shifted_subword(
            outputword, vhdl_dict["ETA1_OUT_LOW"], vhdl_dict["ETA1_OUT_HIGH"])
        eta2 = bithlp.get_shifted_subword(
            outputword, vhdl_dict["ETA2_OUT_LOW"], vhdl_dict["ETA2_OUT_HIGH"])
        return eta1, eta2
