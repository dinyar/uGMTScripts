class Shower():

    """
    A class capable of interpreting the uGMT emulator and hardware muon representations
    Constructed either with a dict of bitwords or a shower object.
    The dict of bitwords should be two long if we're looking at an input shower, or
    four long if we're looking at an output shower.
    """

    def __init__(self, vhdl_dict, shower_type, bitwords={}, obj=None):
        """
        ctor:
        TAKES:
            vhdl_dict    as returned by ../../tools/vhdl.VHDLConstantsParser
            shower_type  either IN (inputs) or OUT (outputs)
            bitwords     either a dict of two bitwords representing an input shower on the link or 
                         a dict of a dict with one or two bitwords representing an output shower on four links
            obj          can be None or one of the emulator objects
        """

        if obj == None: # We got a dict of bitwords
            self.bitwords = bitwords
            if len(bitwords) == 2 and shower_type == "IN":
                self.isOneNominal = (
                    bitwords[vhdl_dict["EMTF_SHOWERS_STD_FRAME"]] >> vhdl_dict["EMTF_SHOWERS_POS0"]) & 1
                self.isOneTight = (
                    bitwords[vhdl_dict["EMTF_SHOWERS_STD_FRAME"]] >> vhdl_dict["EMTF_SHOWERS_POS1"]) & 1
                self.isOneLoose = (
                    bitwords[vhdl_dict["EMTF_SHOWERS_EXT_FRAME"]] >> vhdl_dict["EMTF_SHOWERS_POS0"]) & 1
            elif len(bitwords) == 2 and isinstance(bitwords[vhdl_dict["SHOWERS_OUT_ONE_NOMINAL_LINK"]], dict) and shower_type == "OUT":
                # Position within 32 bit word
                shower_offset = vhdl_dict["SHOWERS_OUT"] - 32
                self.isOneNominal = (
                    bitwords[vhdl_dict["SHOWERS_OUT_ONE_NOMINAL_LINK"]][2+2*vhdl_dict["SHOWERS_OUT_STD_MU"]+1] >> shower_offset) & 1
                self.isOneTight = (
                    bitwords[vhdl_dict["SHOWERS_OUT_ONE_TIGHT_LINK"]][2+2*vhdl_dict["SHOWERS_OUT_STD_MU"]+1] >> shower_offset) & 1
                self.isTwoLooseDiffSectors = (
                    bitwords[vhdl_dict["SHOWERS_OUT_TWO_LOOSE_LINK"]][2+2*vhdl_dict["SHOWERS_OUT_EXT_MU"]+1] >> shower_offset) & 1
            else:
                self.bitwords = {}
                raise TypeError(
                    'When no object is passed `bitwords` should contain either two words if `shower_type` is "IN" or two dictionaries if it is "OUT".')
        elif len(bitwords) == 0 and obj != None:
            self.isOneNominal = obj.isOneNominalInTime()
            self.isOneTight = obj.isOneTightInTime()

            self.bitwords = {}
            if shower_type == "IN":
                self.isOneLoose = obj.isOneLooseInTime()
                self.bitwords[vhdl_dict["EMTF_SHOWERS_STD_FRAME"]] = (
                    obj.isOneNominalInTime() & 1) << vhdl_dict["EMTF_SHOWERS_POS0"]
                self.bitwords[vhdl_dict["EMTF_SHOWERS_STD_FRAME"]] += (
                    obj.isOneTightInTime() & 1) << vhdl_dict["EMTF_SHOWERS_POS1"]
                self.bitwords[vhdl_dict["EMTF_SHOWERS_EXT_FRAME"]] = (
                    obj.isOneLooseInTime() & 1) << vhdl_dict["EMTF_SHOWERS_POS0"]
                self.tftype = obj.trackFinderType()
            elif shower_type == "OUT":
                self.isTwoLooseDiffSectors = obj.isTwoLooseDiffSectorsInTime()
                # Position within 32 bit word
                shower_offset = vhdl_dict["SHOWERS_OUT"] - 32
                self.bitwords[vhdl_dict["SHOWERS_OUT_ONE_NOMINAL_LINK"]] = {}
                self.bitwords[vhdl_dict["SHOWERS_OUT_ONE_TIGHT_LINK"]] = {}
                # Some arthithmetic gymnastics to move showers to the correct frame
                self.bitwords[vhdl_dict["SHOWERS_OUT_ONE_NOMINAL_LINK"]][2+2*vhdl_dict["SHOWERS_OUT_STD_MU"]+1] = (
                    obj.isOneNominalInTime() & 1) << shower_offset
                self.bitwords[vhdl_dict["SHOWERS_OUT_ONE_TIGHT_LINK"]][2+2*vhdl_dict["SHOWERS_OUT_STD_MU"]+1] = (
                    obj.isOneTightInTime() & 1) << shower_offset
                self.bitwords[vhdl_dict["SHOWERS_OUT_TWO_LOOSE_LINK"]][2+2*vhdl_dict["SHOWERS_OUT_EXT_MU"]+1] = (
                    obj.isTwoLooseDiffSectorsInTime() & 1) << shower_offset
            else:
                raise TypeError('`shower_type` can only be "IN" or "OUT".')
