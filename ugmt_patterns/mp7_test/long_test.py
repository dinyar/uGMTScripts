#!/bin/env python

from tools.vhdl import VHDLConstantsParser
from helpers.mp7_buffer_parser import InputBufferParser, OutputBufferParser, Version
import zlib
import argparse
import os
import subprocess
import time
import difflib

import traceback
import sys
sys.path.append('../')


def decompress(pattern_fname, comp_fname):
    with open(comp_fname, 'rb') as ifile:
        with open(pattern_fname, 'wb') as ofile:
            ofile.write(zlib.decompress(ifile.read()))


def parse_options():
    desc = "Interface for multi-buffer test"

    parser = argparse.ArgumentParser(
        description=desc, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('boardname', type=str,
                        default='ugmt_b40', help='name of mp7 board')
    parser.add_argument('--connections_file', '-c', type=str, dest='connections_file',
                        default='file://${MP7_TESTS}/etc/mp7/connections-test.xml;file://${MP7_TESTS}/etc/mp7/connections-RAL.xml;file://${MP7_TESTS}/etc/mp7/connections-oct14.xml;file://${MP7_TESTS}/etc/mp7/connections-calo-2015.xml;file://${MP7_TESTS}/etc/mp7/connections-TDR.xml',
                        help='Path of connections file to be used.')
    parser.add_argument('--energydelay', '-d', type=str, dest='en_delay',
                        default='0', help='Delay for energy deposits with respect to muons')
    parser.add_argument('--dir', type=str, dest='directory',
                        default='patterns/many_events/', help='directory containing compressed patterns')
    parser.add_argument('--fast_fail', default=False, action='store_true',
                        help='fail test as soon as the first mismatches appear.')
    parser.add_argument('--dump', default=False,
                        action='store_true', help='always dump the buffers.')
    parser.add_argument('--dump_on_error', default=False, action='store_true',
                        help='dump buffers when an error was detected.')
    parser.add_argument('--out', type=str, dest='outpath',
                        default='dumps', help='directory for buffer dumps')
    parser.add_argument('--logs', type=str, dest='logpath',
                        default='logs', help='directory to store pattern logs')
    parser.add_argument('--test', type=str, dest='testpath', default='',
                        help='directory containing HW response (suppresses HW access)')

    # parser.add_argument('--details', default=False, action='store_true', help='do detailed analysis of individual muons')
    opts = parser.parse_args()

    return opts


def mean(l):
    return sum(l)/float(len(l))


def variance(l):
    m = mean(l)
    return sum((xi - m)**2 for xi in l) / float(len(l))


class Analyser():
    def __init__(self, emufname, hwfname, vhdcfg):
        self.emufname = emufname
        self.hwfname = hwfname
        self.vhdcfg = vhdcfg

        self.pt_errors = {}
        self.phi_errors = {}
        self.iso_errors = {}
        self.eta_errors = {}
        self.chrg_errors = {}
        self.qual_errors = {}
        self.erroneous_bitwords = {}
        self.erroneous_showerbitwords = {}
        self.erroneous_frames_in_rx_file = {}
        self.erroneous_frames_in_tx_file = {}
        self.erroneous_shower_frames_in_rx_file = {}
        self.erroneous_scouting_frames_in_rx_file = {}
        self.erroneous_shower_frames_in_tx_file = {}
        self.erroneous_scouting_frames_in_tx_file = {}
        self.erroneous_bril_scouting_frames_in_rx_file = {}
        self.erroneous_bril_scouting_frames_in_tx_file = {}
        self.errors = set()
        self.shower_errors = set()
        self.scouting_errors = set()
        self.bril_scouting_errors = set()

        self.emuversion = Version("99_99_99")
        self.hwversion = Version("0_2_19")
        self.hw_bmtf_mus = 0
        self.hw_omtfn_mus = 0
        self.hw_omtfp_mus = 0
        self.hw_emtfn_mus = 0
        self.hw_emtfp_mus = 0
        self.emu_bmtf_mus = 0
        self.emu_omtfn_mus = 0
        self.emu_omtfp_mus = 0
        self.emu_emtfn_mus = 0
        self.emu_emtfp_mus = 0
        self.event_ctr = 0
        self.event_nontrivial_cntr = 0
        self.mu_cntr = 0
        self.shower_onenominal_ctr = 0
        self.shower_onetight_ctr = 0
        self.shower_twoloose_ctr = 0
        self.event_nontrivial_showers_cntr = 0
        self.hw_pt_values = []
        self.hw_pt_unconstr_values = []
        self.hw_phi_values = []
        self.hw_eta_values = []
        self.hw_phi_extrapol_values = []
        self.hw_eta_extrapol_values = []
        self.hw_qual_values = []
        self.hw_charge_values = []
        self.hw_impact_param_values = []
        self.hw_tfLink_values = set()
        self.emu_pt_values = []
        self.emu_pt_unconstr_values = []
        self.emu_phi_values = []
        self.emu_eta_values = []
        self.emu_phi_extrapol_values = []
        self.emu_eta_extrapol_values = []
        self.emu_qual_values = []
        self.emu_charge_values = []
        self.emu_impact_param_values = []
        self.emu_tfLink_values = set()

    def analyse(self, filecnt):
        eventstart = filecnt * 160
        print(("emulator", self.emufname))
        print(("HW", self.hwfname))
        emu_parser = OutputBufferParser(
            self.emufname, self.vhdcfg, self.emuversion)
        hw_parser = OutputBufferParser(
            self.hwfname, self.vhdcfg, self.hwversion, from_firmware=True)
        emushowerout = emu_parser.get_output_showers()
        hwshowerout = hw_parser.get_output_showers()
        emuout = emu_parser.get_output_muons()
        hwout = hw_parser.get_output_muons()
        emuscout = emu_parser.get_scouting_muons()
        hwscout = hw_parser.get_scouting_muons()
        emubrilscout = emu_parser.get_bril_scouting_muons()
        hwbrilscout = hw_parser.get_bril_scouting_muons()
        input_parser = InputBufferParser("rx_tmp.txt", self.vhdcfg)
        input_parser.get_input_muons()
        self.event_ctr += len(emuout) // (8*6)
        showercntr = 0  # running index
        shower_onenominal_nonzero_ctr = 0  # counts how many non-zero
        shower_onetight_nonzero_ctr = 0  # counts how many non-zero
        shower_twoloose_nonzero_ctr = 0  # counts how many non-zero
        errors_detected = False
        for emu, hw in zip(emushowerout, hwshowerout):
            event = showercntr + eventstart
            if(emu.isOneNominal):
                shower_onenominal_nonzero_ctr += 1
            if(emu.isOneTight):
                shower_onetight_nonzero_ctr += 1
            if(emu.isTwoLooseDiffSectors):
                shower_twoloose_nonzero_ctr += 1
            if (emu.bitwords != hw.bitwords):
                if not event in self.shower_errors:
                    curr_event = showercntr
                    self.erroneous_showerbitwords[event] = []
                    self.erroneous_shower_frames_in_rx_file[event] = (filecnt, "{}-{}".format(
                        6*curr_event+input_parser.frame_low, 6*curr_event+5+input_parser.frame_low))
                    self.erroneous_shower_frames_in_tx_file[event] = (
                        filecnt, "{}-{}".format(6*curr_event+hw_parser.frame_low, 6*curr_event+5+hw_parser.frame_low))
                    self.shower_errors.add(event)
                self.erroneous_showerbitwords[event].append(
                    (emu.bitwords, hw.bitwords))
                errors_detected = True
            showercntr += 1
        mucntr = 0  # running index
        for emu, hw in zip(emuscout, hwscout):
            event = mucntr // (8*6) + eventstart
            if (emu.bitword != hw.bitword):
                if not event in self.scouting_errors:
                    curr_event = mucntr//(8*6)
                    self.erroneous_bitwords[event] = []
                    self.erroneous_scouting_frames_in_rx_file[event] = (filecnt, "{}-{}".format(
                        6*curr_event+input_parser.frame_low, 6*curr_event+5+input_parser.frame_low))
                    self.erroneous_scouting_frames_in_tx_file[event] = (
                        filecnt, "{}-{}".format(6*curr_event+hw_parser.frame_low, 6*curr_event+5+hw_parser.frame_low))
                    self.scouting_errors.add(event)
                self.erroneous_bitwords[event].append(
                    (hex(emu.bitword), hex(hw.bitword)))
                errors_detected = True
            mucntr += 1
        mucntr = 0  # running index
        for emu, hw in zip(emubrilscout, hwbrilscout):
            event = mucntr // (8*6) + eventstart
            if (emu.bitword != hw.bitword):
                if not event in self.bril_scouting_errors:
                    curr_event = mucntr//(8*6)
                    self.erroneous_bitwords[event] = []
                    self.erroneous_bril_scouting_frames_in_rx_file[event] = (filecnt, "{}-{}".format(
                        6*curr_event+input_parser.frame_low, 6*curr_event+5+input_parser.frame_low))
                    self.erroneous_bril_scouting_frames_in_tx_file[event] = (
                        filecnt, "{}-{}".format(6*curr_event+hw_parser.frame_low, 6*curr_event+5+hw_parser.frame_low))
                    self.bril_scouting_errors.add(event)
                self.erroneous_bitwords[event].append(
                    (hex(emu.bitword), hex(hw.bitword)))
                errors_detected = True
            mucntr += 1
        mucntr = 0  # running index
        muon_nonzero_ctr = 0  # counts how many non-zero
        for emu, hw in zip(emuout, hwout):
            event = mucntr // (8*6) + eventstart
            if (emu.bitword != 0):
                muon_nonzero_ctr += 1
                if emu.tftype == 2 and emu.tfside == 1:
                    self.emu_emtfp_mus += 1
                elif emu.tftype == 1 and emu.tfside == 1:
                    self.emu_omtfp_mus += 1
                elif emu.tftype == 0:
                    self.emu_bmtf_mus += 1
                elif emu.tftype == 1 and emu.tfside == -1:
                    self.emu_omtfn_mus += 1
                elif emu.tftype == 2 and emu.tfside == -1:
                    self.emu_emtfn_mus += 1
                self.emu_pt_values.append(emu.ptBits)
                self.emu_pt_unconstr_values.append(emu.ptUnconstrBits)
                self.emu_phi_values.append(emu.phiBits)
                self.emu_eta_values.append(emu.etaBits)
                self.emu_phi_extrapol_values.append(emu.phi_extrapol)
                self.emu_eta_extrapol_values.append(emu.eta_extrapol)
                self.emu_qual_values.append(emu.qualityBits)
                self.emu_charge_values.append(emu.Sysign)
                self.emu_impact_param_values.append(emu.impactParam)
                self.emu_tfLink_values.add(emu.tfMuonIndex//3)
            if (hw.bitword != 0):
                if hw.tftype == 2 and hw.tfside == 1:
                    self.hw_emtfp_mus += 1
                elif hw.tftype == 1 and hw.tfside == 1:
                    self.hw_omtfp_mus += 1
                elif hw.tftype == 0:
                    self.hw_bmtf_mus += 1
                elif hw.tftype == 1 and hw.tfside == -1:
                    self.hw_omtfn_mus += 1
                elif hw.tftype == 2 and hw.tfside == -1:
                    self.hw_emtfn_mus += 1
                self.hw_pt_values.append(hw.ptBits)
                self.hw_pt_unconstr_values.append(hw.ptUnconstrBits)
                self.hw_phi_values.append(hw.phiBits)
                self.hw_eta_values.append(hw.etaBits)
                self.hw_phi_extrapol_values.append(hw.phi_extrapol)
                self.hw_eta_extrapol_values.append(hw.eta_extrapol)
                self.hw_qual_values.append(hw.qualityBits)
                self.hw_charge_values.append(hw.Sysign)
                self.hw_impact_param_values.append(hw.impactParam)
                self.hw_tfLink_values.add(hw.tfMuonIndex//3)
            # For outputs eta isn't part of the bitword anymore.
            if (emu.bitword != hw.bitword) or (emu.etaBits != hw.etaBits):
                errors_detected = True
                if not event in self.errors:
                    self.erroneous_frames_in_rx_file[event] = (filecnt, "{}-{}".format(
                        6*(mucntr//(8*6))+input_parser.frame_low, 6*(mucntr//(8*6))+5+input_parser.frame_low))
                    self.erroneous_frames_in_tx_file[event] = (filecnt, "{}-{}".format(
                        6*(mucntr//(8*6))+hw_parser.frame_low, 6*(mucntr//(8*6))+5+hw_parser.frame_low))
                    self.erroneous_bitwords[event] = []
                    self.errors.add(event)
                    self.pt_errors[event] = 0
                    self.phi_errors[event] = 0
                    self.iso_errors[event] = 0
                    self.eta_errors[event] = 0
                    self.chrg_errors[event] = 0
                    self.qual_errors[event] = 0
                self.erroneous_bitwords[event].append(
                    (hex(emu.bitword), hex(hw.bitword)))
                if emu.ptBits != hw.ptBits:
                    self.pt_errors[event] += 1
                if emu.phiBits != hw.phiBits:
                    self.phi_errors[event] += 1
                if emu.etaBits != hw.etaBits:
                    self.eta_errors[event] += 1
                if emu.Iso != hw.Iso:
                    self.iso_errors[event] += 1
                if emu.Sysign != hw.Sysign:
                    self.chrg_errors[event] += 1
                if emu.qualityBits != hw.qualityBits:
                    self.qual_errors[event] += 1
            mucntr += 1
            if mucntr % (8*6) == 0:  # for every event check
                self.mu_cntr += muon_nonzero_ctr
                if muon_nonzero_ctr > 0:
                    self.event_nontrivial_cntr += 1
                muon_nonzero_ctr = 0
                self.shower_onenominal_ctr += shower_onenominal_nonzero_ctr
                self.shower_onetight_ctr += shower_onetight_nonzero_ctr
                self.shower_twoloose_ctr += shower_twoloose_nonzero_ctr
                if shower_onenominal_nonzero_ctr > 0 or shower_onetight_nonzero_ctr > 0 or shower_twoloose_nonzero_ctr > 0:
                    self.event_nontrivial_showers_cntr += 1
                shower_onenominal_nonzero_ctr = 0
                shower_onetight_nonzero_ctr = 0
                shower_twoloose_nonzero_ctr = 0
        return errors_detected

    def get_num_shower_errors(self):
        return len(self.shower_errors)

    def get_num_errors(self):
        return len(self.errors)

    def get_num_scouting_errors(self):
        return len(self.scouting_errors)

    def get_num_bril_scouting_errors(self):
        return len(self.bril_scouting_errors)

    def get_total(self, cntr_dict):
        evts = []
        total = 0
        for ev, n in cntr_dict.items():
            total += n
            if not ev in evts and n != 0:
                evts.append(ev)
        return total, evts

    def all_tfs_tested(self):
        if self.emu_emtfp_mus != 0 and self.emu_omtfp_mus != 0 and self.emu_emtfn_mus != 0 and self.emu_omtfn_mus != 0 and self.emu_bmtf_mus != 0 and self.hw_emtfp_mus != 0 and self.hw_omtfp_mus != 0 and self.hw_emtfn_mus != 0 and self.hw_omtfn_mus != 0 and self.hw_bmtf_mus != 0:
            return True
        else:
            return False

    def input_channels_untested(self, channel_set):
        truth = set(range(self.vhdcfg["NUM_MU_CHANS"]))
        return truth - channel_set

    def all_input_channels_tested(self):
        if (len(self.input_channels_untested(self.emu_tfLink_values)) == 0) and (len(self.input_channels_untested(self.emu_tfLink_values)) == 0):
            return True
        else:
            return False

    def variance_large_enough(self):
        pt_cut = 10
        phi_cut = 200
        eta_cut = 100
        qual_cut = 0
        charge_cut = 0
        ip_cut = 0
        var_check = variance(self.hw_pt_values) > pt_cut and variance(self.hw_pt_unconstr_values) > pt_cut and variance(self.hw_phi_values) > phi_cut and variance(self.hw_eta_values) > eta_cut and variance(self.hw_phi_extrapol_values) > phi_cut and variance(self.hw_eta_extrapol_values) > eta_cut and variance(self.hw_qual_values) > qual_cut and variance(self.hw_charge_values) > charge_cut and variance(self.hw_impact_param_values) > ip_cut and variance(
            self.emu_pt_values) > pt_cut and variance(self.emu_pt_unconstr_values) > pt_cut and variance(self.emu_phi_values) > phi_cut and variance(self.emu_eta_values) > eta_cut and variance(self.emu_phi_extrapol_values) > phi_cut and variance(self.emu_eta_extrapol_values) > eta_cut and variance(self.emu_qual_values) > qual_cut and variance(self.emu_charge_values) > charge_cut and variance(self.emu_impact_param_values) > ip_cut
        if var_check:
            return True
        else:
            return False

    def print_out(self, pattern, logger=None):
        print_statements = []
        if logger:
            logger.write("Errors in pattern {pat}\n".format(pat=pattern))

        print_statements.append(
            "error summary for pattern: {}".format(pattern))
        print_statements.append(
            "-----------------------------------------------------------------------")
        print_statements.append(
            "Note counters: nerrors / total instances (non-trivial instances)")
        print_statements.append(
            "-----------------------------------------------------------------------")
        if logger:
            logger.write(
                "-----------------------------------------------------------------------\n")
            logger.write("Sample summary: \n")
            logger.write("Number of EMTFp muons: {} emu and {} hw\n".format(
                self.emu_emtfp_mus, self.hw_emtfp_mus))
            logger.write("Number of OMTFp muons: {} emu and {} hw\n".format(
                self.emu_omtfp_mus, self.hw_omtfp_mus))
            logger.write("Number of BMTF muons: {} emu and {} hw\n".format(
                self.emu_bmtf_mus, self.hw_bmtf_mus))
            logger.write("Number of OMTFn muons: {} emu and {} hw\n".format(
                self.emu_omtfn_mus, self.hw_omtfn_mus))
            logger.write("Number of EMTFn muons: {} emu and {} hw\n\n".format(
                self.emu_emtfn_mus, self.hw_emtfn_mus))
            logger.write("Sample statistics: \n")
            logger.write("pT mean and variance: {} ({}) for emu and {} ({}) for hw\n".format(mean(
                self.emu_pt_values), variance(self.emu_pt_values), mean(self.hw_pt_values), variance(self.hw_pt_values)))
            logger.write("pT unconstrained mean and variance: {} ({}) for emu and {} ({}) for hw\n".format(mean(self.emu_pt_unconstr_values), variance(
                self.emu_pt_unconstr_values), mean(self.hw_pt_unconstr_values), variance(self.hw_pt_unconstr_values)))
            logger.write("phi mean and variance: {} ({}) for emu and {} ({}) for hw\n".format(mean(
                self.emu_phi_values), variance(self.emu_phi_values), mean(self.hw_phi_values), variance(self.hw_phi_values)))
            logger.write("eta mean and variance: {} ({}) for emu and {} ({}) for hw\n".format(mean(
                self.emu_eta_values), variance(self.emu_eta_values), mean(self.hw_eta_values), variance(self.hw_eta_values)))
            logger.write("extrapolated phi mean and variance: {} ({}) for emu and {} ({}) for hw\n".format(mean(self.emu_phi_extrapol_values), variance(
                self.emu_phi_extrapol_values), mean(self.hw_phi_extrapol_values), variance(self.hw_phi_extrapol_values)))
            logger.write("extrapolated eta mean and variance: {} ({}) for emu and {} ({}) for hw\n".format(mean(self.emu_eta_extrapol_values), variance(
                self.emu_eta_extrapol_values), mean(self.hw_eta_extrapol_values), variance(self.hw_eta_extrapol_values)))
            logger.write("quality mean and variance: {} ({}) for emu and {} ({}) for hw\n".format(mean(
                self.emu_qual_values), variance(self.emu_qual_values), mean(self.hw_qual_values), variance(self.hw_qual_values)))
            logger.write("charge mean and variance: {} ({}) for emu and {} ({}) for hw\n".format(mean(
                self.emu_charge_values), variance(self.emu_charge_values), mean(self.hw_charge_values), variance(self.hw_charge_values)))
            logger.write("impact parameter mean and variance: {} ({}) for emu and {} ({}) for hw\n".format(mean(self.emu_impact_param_values), variance(
                self.emu_impact_param_values), mean(self.hw_impact_param_values), variance(self.hw_impact_param_values)))
            logger.write("input channels never contributing to outputs: {} for emu and {} for hw.\n".format(sorted(
                self.input_channels_untested(self.emu_tfLink_values)), sorted(self.input_channels_untested(self.hw_tfLink_values))))
            logger.write(
                "-----------------------------------------------------------------------\n\n")
            logger.write("number of events with errors: {n} / {tot} ({nontriv})\n".format(
                n=self.get_num_errors(), tot=self.event_ctr, nontriv=self.event_nontrivial_cntr))
        print_statements.append("number of events with shower errors: {n} / {tot} ({nontriv})".format(
            n=self.get_num_shower_errors(), tot=self.event_ctr, nontriv=self.event_nontrivial_showers_cntr))
        print_statements.append("number of instances of OneNominal: {on}, OneTight: {ot}, TwoLoose: {tl}".format(
            on=self.shower_onenominal_ctr, ot=self.shower_onetight_ctr, tl=self.shower_twoloose_ctr))
        print_statements.append("number of events with scouting errors: {n} / {tot} ({nontriv})".format(
            n=self.get_num_scouting_errors(), tot=self.event_ctr, nontriv=self.event_nontrivial_cntr))
        print_statements.append("number of events with BRIL scouting errors: {n} / {tot} ({nontriv})".format(
            n=self.get_num_bril_scouting_errors(), tot=self.event_ctr, nontriv=self.event_nontrivial_cntr))
        print_statements.append("number of events with output muon errors: {n} / {tot} ({nontriv})".format(
            n=self.get_num_errors(), tot=self.event_ctr, nontriv=self.event_nontrivial_cntr))
        if logger:
            logger.write("erroneous shower events: {evts}\n".format(
                evts=self.shower_errors))
            logger.write("erroneous shower bitwords (emu vs. fw): {}\n".format(
                self.erroneous_showerbitwords))
            logger.write("location of erroneous shower frames in TX buffer: {}\n".format(
                self.erroneous_shower_frames_in_tx_file))
            logger.write("expected location of erroneous shower frames in RX buffer: {}\n".format(
                self.erroneous_shower_frames_in_rx_file))
            logger.write("erroneous events: {evts}\n".format(evts=self.errors))
            logger.write("erroneous scouting events: {evts}\n".format(
                evts=self.scouting_errors))
            logger.write("erroneous BRIL scouting events: {evts}\n".format(
                evts=self.bril_scouting_errors))
            logger.write("erroneous bitwords (emu vs. fw): {}\n".format(
                self.erroneous_bitwords))
            logger.write("location of erroneous frames in TX buffer: {}\n".format(
                self.erroneous_frames_in_tx_file))
            logger.write("expected location of erroneous frames in RX buffer: {}\n".format(
                self.erroneous_frames_in_rx_file))
            logger.write("location of erroneous scouting frames in TX buffer: {}\n".format(
                self.erroneous_scouting_frames_in_tx_file))
            logger.write("expected location of erroneous scouting frames in RX buffer: {}\n".format(
                self.erroneous_scouting_frames_in_rx_file))
            logger.write("location of erroneous BRIL scouting frames in TX buffer: {}\n".format(
                self.erroneous_bril_scouting_frames_in_tx_file))
            logger.write("expected location of erroneous BRIL scouting frames in RX buffer: {}\n".format(
                self.erroneous_bril_scouting_frames_in_rx_file))
        tot, events = self.get_total(self.pt_errors)
        print_statements.append(
            "number of pt errors:   {} / {}".format(tot, self.mu_cntr))
        if logger:
            logger.write(
                "number of muons with pt errors: {n} / {total}\n".format(n=tot, total=self.mu_cntr))
            logger.write("erroneous events: {evts}\n".format(evts=events))

        tot, events = self.get_total(self.phi_errors)
        print_statements.append(
            "number of phi errors:  {} / {}".format(tot, self.mu_cntr))
        if logger:
            logger.write(
                "number of muons with phi errors: {n} / {total}\n".format(n=tot, total=self.mu_cntr))
            logger.write("erroneous events: {evts}\n".format(evts=events))

        tot, events = self.get_total(self.eta_errors)
        print_statements.append(
            "number of eta errors:  {} / {}".format(tot, self.mu_cntr))
        if logger:
            logger.write(
                "number of muons with eta errors: {n} / {total}\n".format(n=tot, total=self.mu_cntr))
            logger.write("erroneous events: {evts}\n".format(evts=events))

        tot, events = self.get_total(self.qual_errors)
        print_statements.append(
            "number of qual errors: {} / {}".format(tot, self.mu_cntr))
        if logger:
            logger.write(
                "number of muons with qual errors: {n} / {total}\n".format(n=tot, total=self.mu_cntr))
            logger.write("erroneous events: {evts}\n".format(evts=events))

        tot, events = self.get_total(self.chrg_errors)
        print_statements.append(
            "number of chrg errors: {} / {}".format(tot, self.mu_cntr))
        if logger:
            logger.write(
                "number of muons with chrg errors: {n} / {total}\n".format(n=tot, total=self.mu_cntr))
            logger.write("erroneous events: {evts}\n".format(evts=events))

        tot, events = self.get_total(self.iso_errors)
        print_statements.append(
            "number of iso errors:  {} / {}".format(tot, self.mu_cntr))
        if logger:
            logger.write(
                "number of muons with iso errors: {n} / {total}\n".format(n=tot, total=self.mu_cntr))
            logger.write("erroneous events: {evts}\n".format(evts=events))
        print_statements.append(
            "-----------------------------------------------------------------------")
        if logger:
            print_statements.append("for more info see log file.")
        return '\n'.join(print_statements)


def check_pattern(opts, patterns_path, log_path):
    file_list_rx = []
    file_list_tx = []
    pattern_name = ""

    for root, dirs, files in os.walk(patterns_path):
        for fname in files:
            if fname.startswith('tx_') and fname.endswith('.zip'):
                # strip leading tx_ and _<i>.zip
                pattern_name = fname[3:-6]
                file_list_tx.append(root + "/" + fname)
            if fname.startswith('rx_') and fname.endswith('.zip'):
                file_list_rx.append(root + "/" + fname)

    if len(file_list_rx) == 0:
        raise RuntimeError("No input files found. Aborting!")

    known_responses = []
    if opts.testpath != '':
        for root, dirs, files in os.walk(opts.testpath):
            for fname in files:
                if fname.startswith('tx_'):
                    known_responses.append(root + fname)

    # this should make them synchronised
    file_list_rx.sort()
    file_list_tx.sort()
    known_responses.sort()

    if len(file_list_rx) != len(file_list_tx):
        print("Number of RX and TX files not the same!")
        return

    print(("-" * 30, pattern_name, "-" * 30))
    print(("Starting test: A total of {i} files will be fed into the algorithm".format(i=len(file_list_rx))))
    log = open('{path}/{pattern}.log'.format(path=log_path, pattern=pattern_name), 'w')

    vhdl_cfg = VHDLConstantsParser.parse_vhdl_file("data/ugmt_constants.vhd")
    analyser = Analyser('tx_tmp.txt', 'tmp/tx_summary.txt', vhdl_cfg)

    # setting clock source to internal
    try:
        subprocess.check_output(['mp7butler.py', '-c', opts.connections_file, 'reset',
                                opts.boardname, '--clksrc', 'internal'], stderr=subprocess.STDOUT)
        subprocess.check_output(['mp7butler.py', '-c', opts.connections_file, 'formatters',
                                opts.boardname, '--dmx-hdrfmt', 'no-strip,insert'], stderr=subprocess.STDOUT)
    except subprocess.CalledProcessError as e:
        print(("MP7butler exited with error {}: {}".format(e.returncode, e.output)))
        raise

    try:
        for i in range(len(file_list_rx)):
            print(('Processing file {idx}, {rxFile}'.format(idx=i + 1, rxFile=file_list_rx[i])))
            cmp_rx = file_list_rx[i]
            cmp_tx = file_list_tx[i]
            rx_tmp_name = 'rx_tmp.txt'
            tx_tmp_name = 'tx_tmp.txt'
            decompress(rx_tmp_name, cmp_rx)
            decompress(tx_tmp_name, cmp_tx)

            cap_lines = []
            if opts.testpath == '':
                try:
                    subprocess.check_output(['mp7butler.py', '-c',
                                             opts.connections_file,
                                             'buffers', opts.boardname,
                                             'algoPlay', '--inject',
                                             'file://{fname}'.format(
                                                 fname=rx_tmp_name),
                                             '-e', '36-71'],
                                            stderr=subprocess.STDOUT)
                    subprocess.check_output(['mp7butler.py', '-c',
                                             opts.connections_file,
                                             'buffers', opts.boardname,
                                             'algoPlay', '--inject',
                                             'file://{fname}'.format(
                                                 fname=rx_tmp_name),
                                             '-e', '0-35', '--play',
                                             '{delay}'.format(delay=opts.en_delay)],
                                            stderr=subprocess.STDOUT)
                except subprocess.CalledProcessError as e:
                    print(("MP7butler exited with error {}: {}".format(e.returncode, e.output)))
                    raise
                time.sleep(1)
                try:
                    subprocess.check_output(['mp7butler.py', '-c',
                                             opts.connections_file, 'capture',
                                             opts.boardname, '--outputpath',
                                             'tmp'], stderr=subprocess.STDOUT)
                except subprocess.CalledProcessError as e:
                    print(("MP7butler exited with error {}: {}".format(e.returncode, e.output)))
                    raise
            else:
                with open(known_responses[i], 'r') as cap:
                    cap_lines = cap.readlines()
                analyser.hwfname = known_responses[i]

            have_errors = analyser.analyse(i)

            # print current summary for subset of files
            if i % 10 == 9:
                print((analyser.print_out(pattern_name)))

            # Store buffer dumps if errors are detected
            if opts.dump or (opts.dump_on_error and have_errors):
                # Create directory if it doesn't exist
                if opts.dump or opts.dump_on_error:
                    if not os.path.exists(opts.outpath):
                        os.makedirs(opts.outpath)

                try:
                    subprocess.check_output(['cp', 'tmp/tx_summary.txt',
                                             opts.outpath + '/tx_summary_hw_{x}.txt'.format(x=i)])
                    subprocess.check_output(['cp', 'tmp/rx_summary.txt',
                                             opts.outpath + '/rx_summary_hw_{x}.txt'.format(x=i)])
                    subprocess.check_output(['cp', 'tx_tmp.txt',
                                             opts.outpath + '/tx_summary_emu_{x}.txt'.format(x=i)])
                    subprocess.check_output(['cp', 'rx_tmp.txt',
                                             opts.outpath + '/rx_summary_emu_{x}.txt'.format(x=i)])

                except subprocess.CalledProcessError as e:
                    print(("cp exited with error {}: {}".format(e.returncode, e.output)))
                    raise
            if opts.fast_fail and have_errors:
                print("Errors detected and fast fail requested. Exiting.. ")
                break

    except KeyboardInterrupt:
        print(('Keyboard interrupt while processing file {idx}, {rxFile}'.format(idx=i + 1, rxFile=file_list_rx[i])))

    # print final summary and write details to log
    analyser_summary = analyser.print_out(pattern_name, log)
    log.close()
    return analyser_summary, analyser.get_num_errors(), analyser.get_num_scouting_errors(), analyser.get_num_bril_scouting_errors(), analyser.all_tfs_tested(), analyser.all_input_channels_tested(), analyser.variance_large_enough()


def main():
    opts = parse_options()
    if not os.path.exists(opts.logpath):
        os.makedirs(opts.logpath)

    analysis_results = []
    num_tot_errors = 0
    num_tot_scouting_errors = 0
    num_tot_bril_scouting_errors = 0
    all_tfs_tested_once = False
    all_input_chans_tested_once = False
    var_large_enough_once = False
    for f in os.listdir(opts.directory):
        patterns_path = os.path.join(opts.directory, f)
        if os.path.isdir(patterns_path):
            summary, num_errors, num_scouting_errors, num_bril_scouting_errors, all_tfs_tested, all_input_chans_tested, variance_large_enough = check_pattern(
                opts, patterns_path, opts.logpath)
            analysis_results.append(summary)
            num_tot_errors += num_errors
            num_tot_scouting_errors += num_scouting_errors
            num_tot_bril_scouting_errors += num_bril_scouting_errors
            all_tfs_tested_once |= all_tfs_tested
            all_input_chans_tested_once |= all_input_chans_tested
            var_large_enough_once |= variance_large_enough
            if num_errors > 0:
                break

    print(('\n\n'.join(analysis_results)))

    if num_tot_errors > 0:
        raise RuntimeError(
            'Detected mismatches between firmware and expected results.')
    if num_tot_scouting_errors > 0:
        raise RuntimeError(
            'Detected mismatches between firmware and expected results for the scouting outputs.')
    if num_tot_bril_scouting_errors > 0:
        raise RuntimeError(
            'Detected mismatches between firmware and expected results for the BRIL scouting outputs.')
    if not all_tfs_tested_once:
        raise RuntimeError('Not all track finders were tested!')
    if not all_input_chans_tested_once:
        raise RuntimeError(
            'Did not find muons from all input channels in outputs!')
    if not var_large_enough_once:
        raise RuntimeError(
            'Variance of one of the muon variable not large enough!')


if __name__ == "__main__":
    try:
        main()
        sys.exit(0)
    except RuntimeError as e:
        print(("ERROR: Completed without success: {}".format(e)))
        print((traceback.format_exc()))
        sys.exit(1)
    except Exception as e:
        print(("Unexpected error occured: {}".format(repr(e))))
        print((traceback.format_exc()))
        sys.exit(1)
