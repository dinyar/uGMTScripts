from helpers.options import parse_options, discover_emu_files
from tools.vhdl import VHDLConstantsParser
from DataFormats.FWLite import Events, Handle
from ROOT import l1t
import zlib
import time
from ROOT import gSystem, gROOT
from helpers.pattern_dumper import PatternDumper, BufferWriter
from helpers.muon import Muon
from helpers.shower import Shower
from collections import deque
import sys
import subprocess
sys.path.append('../')


# ROOT


def setupROOT():
    gSystem.Load("libFWCoreFWLite")
    gROOT.ProcessLine('FWLiteEnabler::enable();')
    # gSystem.Load("libCintex")
    # gROOT.ProcessLine('ROOT::Cintex::Cintex::Enable();')


setupROOT()
# gSystem.Load("libL1TriggerL1TMuon")


# CMSSW
#from L1Trigger.L1TGlobalMuon import MicroGMTRankPtQualLUT
# ../tools:


def get_muon_list_empty(vhdl_dict, mu_type, nexpected=8):
    return [Muon(vhdl_dict, mu_type, bitword=0, eta_out=0)]*nexpected


def get_muon_list_out(emu_product, mu_type, vhdl_dict, nexpected=8):
    mulist = get_muon_list_empty(vhdl_dict, mu_type, nexpected)
    for i in range(emu_product.size(0)):
        if emu_product.at(0, i).hwPt() > 0:
            mu_tmp = Muon(vhdl_dict, mu_type, obj=emu_product.at(0, i))
            mulist[i] = mu_tmp
    return mulist


def get_shower_out(emu_product, vhdl_dict):
    showers = get_dummy_out_shower(vhdl_dict)
    if emu_product.size(0) > 0:
        showers = [Shower(vhdl_dict, "OUT", obj=emu_product.at(0, 0))] # Only one shower in uGMT output
    return showers

def get_dummy_out_shower(vhdl_dict):
    bitwords = {}
    bitwords[vhdl_dict["SHOWERS_OUT_ONE_NOMINAL_LINK"]] = {}
    bitwords[vhdl_dict["SHOWERS_OUT_ONE_TIGHT_LINK"]] = {}
    bitwords[vhdl_dict["SHOWERS_OUT_TWO_LOOSE_LINK"]] = {}
    bitwords[vhdl_dict["SHOWERS_OUT_ONE_NOMINAL_LINK"]][2+2*vhdl_dict["SHOWERS_OUT_STD_MU"]+1] = 0
    bitwords[vhdl_dict["SHOWERS_OUT_ONE_TIGHT_LINK"]][2+2*vhdl_dict["SHOWERS_OUT_STD_MU"]+1] = 0
    bitwords[vhdl_dict["SHOWERS_OUT_TWO_LOOSE_LINK"]][2+2*vhdl_dict["SHOWERS_OUT_EXT_MU"]+1] = 0
    showers = [Shower(vhdl_dict, "OUT", bitwords=bitwords)]
    return showers

def get_muon_list(emu_product, mu_type, vhdl_dict, bx, check=False):
    nexpected = 18
    if mu_type == "BMTF":
        nexpected = 36

    mulist = get_muon_list_empty(vhdl_dict, mu_type="IN", nexpected=nexpected)

    for i in range(emu_product.size(0)):
        mu_tmp = Muon(vhdl_dict, mu_type="IN", obj=emu_product.at(0, i))
        # only take muons from the right side of the detector
        if mu_type.endswith("POS") and mu_tmp.etaBits < 0:
            continue
        if mu_type.endswith("NEG") and mu_tmp.etaBits > 0:
            continue

        # because we don't book all 72*3 muons but only 18*3/36*3
        loc_link = emu_product.at(0, i).processor()
        if mulist[loc_link*3].ptBits == 0:
            mu_tmp.setBunchCounter(0)
            mulist[loc_link*3] = mu_tmp
        elif mulist[loc_link*3+1].ptBits == 0:
            mu_tmp.setBunchCounter(1)
            mulist[loc_link*3+1] = mu_tmp
        elif mulist[loc_link*3+2].ptBits == 0:
            mu_tmp.setBunchCounter(2)
            mulist[loc_link*3+2] = mu_tmp

        if check:
            if mu_tmp.ptBits < 0 or mu_tmp.ptBits > 511:
                print("+++ err > pt out of bounds")
            if mu_tmp.etaBits < -224 or mu_tmp.etaBits > 223:
                print("+++ err > eta out of bounds")
            if mu_tmp.phiBits < 0 or mu_tmp.phiBits > 575:
                print("+++ err > phi out of bounds")
            if mu_tmp.qualityBits < 0 or mu_tmp.qualityBits > 15:
                print("+++ err > quality out of bounds")

    return mulist


def get_shower_list(emu_product, shower_type, vhdl_dict, bx, check=False):
    nexpected = 6

    bitwords = {}
    bitwords[vhdl_dict["EMTF_SHOWERS_STD_FRAME"]] = 0
    bitwords[vhdl_dict["EMTF_SHOWERS_EXT_FRAME"]] = 0
    showerlist = [Shower(vhdl_dict, shower_type="IN",
                         bitwords=bitwords)]*nexpected

    for i in range(emu_product.size(0)):
        shower_tmp = Shower(vhdl_dict, shower_type="IN",
                            obj=emu_product.at(0, i))
        if shower_type.endswith("NEG") and shower_tmp.tftype != 3:
            continue
        if shower_type.endswith("POS") and shower_tmp.tftype != 4:
            continue

        showerlist[emu_product.at(0, i).processor()] = shower_tmp

    return showerlist


def get_calo_list():
    calo_sums = [0]*36*28
    # for i in xrange(raw_sums.size(0)):
    #     idx = raw_sums.at(0, i).hwPhi() + raw_sums.at(0, i).hwEta()*36
    #     # print raw_sums.at(0, i).hwPhi(), raw_sums.at(0, i).hwEta()
    #     calo_sums[idx] = raw_sums.at(0, i).etBits()
    return calo_sums


def dump_files(directory, fname, n, input_buffer, output_buffer, indelay, outdelay):
    with open('{path}/rx_{fname}_{idx}.zip'.format(path=directory, fname=fname, idx=n), 'wb') as ofile:
        ofile.write(zlib.compress(input_buffer.dump_string().encode("ascii")))
    with open('{path}/tx_{fname}_{idx}.zip'.format(path=directory, fname=fname, idx=n), 'wb') as ofile:
        ofile.write(zlib.compress(output_buffer.dump_string(True).encode("ascii")))
    output_buffer.writeEmptyFrames(outdelay)
    if indelay > 0:
        input_buffer.writeEmptyFrames(indelay)


def drain_scoutingmuons_queue(vhdl_dict, output_buffer, final_scoutingmuons_queue, imd_bmtf_scouting_muons_queue, final_scoutingshowers_queue):
    # Queues should be of equal length, but if not I'd prefer the crash.
    while final_scoutingmuons_queue or imd_bmtf_scouting_muons_queue or final_scoutingshowers_queue:
        outmuons = get_muon_list_empty(vhdl_dict, "OUT", 8)
        imdmuons = get_muon_list_empty(vhdl_dict, "OUT", 8)
        final_scoutingmuons = final_scoutingmuons_queue.pop()
        imd_bmtf_scouting_muons = imd_bmtf_scouting_muons_queue.pop()
        final_scoutingshowers = final_scoutingshowers_queue.pop()
        output_buffer.writeFrameBasedOutputBX(outmuons, imdmuons, trigger_valid=False,
                                              scouting_final_muons=final_scoutingmuons, scouting_bmtf_muons=imd_bmtf_scouting_muons, scouting_final_showers=final_scoutingshowers)


def main():
    vhdl_dict = VHDLConstantsParser.parse_vhdl_file(
        "../data/ugmt_constants.vhd")

    opts = parse_options()
    nSkip = opts.skip
    fname_dict = discover_emu_files(opts.emudirectory)
    # rankLUT = l1t.MicroGMTRankPtQualLUT()

    ALGODELAY = opts.delay + 27  # first frame with valid = 1

    max_events = int((1024-ALGODELAY)/6)-vhdl_dict["SCOUTING_DELAY"]

    for pattern, fnames in fname_dict.items():
        print("+"*30, pattern, "+"*30)
        events = Events(fnames['root'])

        start = time.time()

        out_handle = Handle('BXVector<l1t::Muon>')
        final_scouting_handle = Handle('BXVector<l1t::Muon>')
        imd_bmtf_scouting_handle = Handle('BXVector<l1t::Muon>')
        imd_bmtf_handle = Handle('BXVector<l1t::Muon>')
        imd_emtf_p_handle = Handle('BXVector<l1t::Muon>')
        imd_emtf_n_handle = Handle('BXVector<l1t::Muon>')
        imd_omtf_p_handle = Handle('BXVector<l1t::Muon>')
        imd_omtf_n_handle = Handle('BXVector<l1t::Muon>')
        bar_handle = Handle('BXVector<l1t::RegionalMuonCand>')
        ovl_handle = Handle('BXVector<l1t::RegionalMuonCand>')
        fwd_handle = Handle('BXVector<l1t::RegionalMuonCand>')

        outShower_handle = Handle('BXVector<l1t::MuonShower>')
        fwdShowers_handle = Handle('BXVector<l1t::RegionalMuonShower>')

        calo_handle = Handle('BXVector<l1t::MuonCaloSum>')

        final_scoutingmuons_queue = deque()
        imd_bmtf_scoutingmuons_queue = deque()
        # imd_emtf_brilscoutingmuons_queue = deque()  # May be added later
        final_scoutingshowers_queue = deque()

        basedir_mp7 = "../data/patterns/compressed/"
        path = '{path}/{pattern}/'.format(path=basedir_mp7, pattern=pattern)
        subprocess.check_output(['mkdir', '-p', path])

        input_buffer = PatternDumper(
            path+pattern+".txt", vhdl_dict, BufferWriter)
        output_buffer = PatternDumper(
            path+pattern+"_out.txt", vhdl_dict, BufferWriter)

        if opts.delay > 0:
            input_buffer.writeEmptyFrames(opts.delay)

        setup_time = time.time() - start

        avg_get_label_time = 0
        avg_conversion_time = 0
        avg_write_time = 0

        output_buffer.writeEmptyFrames(ALGODELAY)
        cntr = 0
        local_frame_cntr = 0
        for i, event in enumerate(events):
            if i < nSkip:
                continue

            # print 'event: {evt} {evNr}'.format(evt=i, evNr=event.eventAuxiliary().event())

            evt_start = time.time()
            event.getByLabel("simGmtStage2Digis", out_handle)
            event.getByLabel("simGmtStage2Digis",
                             "imdMuonsBMTF", imd_bmtf_handle)
            event.getByLabel("simGmtStage2Digis",
                             "imdMuonsEMTFPos", imd_emtf_p_handle)
            event.getByLabel("simGmtStage2Digis",
                             "imdMuonsEMTFNeg", imd_emtf_n_handle)
            event.getByLabel("simGmtStage2Digis",
                             "imdMuonsOMTFPos", imd_omtf_p_handle)
            event.getByLabel("simGmtStage2Digis",
                             "imdMuonsOMTFNeg", imd_omtf_n_handle)
            event.getByLabel("simKBmtfDigis", "BMTF", bar_handle)
            event.getByLabel("simOmtfDigis", "OMTF", ovl_handle)
            event.getByLabel("simEmtfDigis", "EMTF", fwd_handle)
            #event.getByLabel("gmtStage2Digis", "BMTF", bar_handle)
            #event.getByLabel("gmtStage2Digis", "EMTF", fwd_handle)
            #event.getByLabel("gmtStage2Digis", "OMTF", ovl_handle)

            event.getByLabel("simGmtShowerDigis", outShower_handle)
            event.getByLabel("simEmtfShowers", "EMTF", fwdShowers_handle)

            # event.getByLabel("simGmtCaloSumDigis", "TriggerTowerSums", calo_handle)
            #event.getByLabel("emptyCaloCollsProducer", "EmptyTriggerTowerSums", calo_handle)
            #event.getByLabel("simGmtCaloSumDigis", "TriggerTower2x2s", calo_handle)
            get_label_time = time.time() - evt_start
            # calo_sums_raw = calo_handle.product()
            calo_sums = get_calo_list()

            emu_out_muons = out_handle.product()
            outmuons = get_muon_list_out(emu_out_muons, "OUT", vhdl_dict)
            scoutmuons_final = get_muon_list_out(
                emu_out_muons, "SCOUT", vhdl_dict)
            imd_emtf_p_prod = imd_emtf_p_handle.product()
            imdmuons = get_muon_list_out(imd_emtf_p_prod, "IMD", vhdl_dict, 4)
            imd_omtf_p_prod = imd_omtf_p_handle.product()
            imdmuons += get_muon_list_out(imd_omtf_p_prod, "IMD", vhdl_dict, 4)
            imd_bmtf_prod = imd_bmtf_handle.product()
            imdmuons += get_muon_list_out(imd_bmtf_prod, "IMD", vhdl_dict, 8)
            scoutmuons_imd = get_muon_list_out(
                imd_bmtf_prod, "SCOUT", vhdl_dict, 8)
            imd_omtf_n_prod = imd_omtf_n_handle.product()
            imdmuons += get_muon_list_out(imd_omtf_n_prod, "IMD", vhdl_dict, 4)
            imd_emtf_n_prod = imd_emtf_n_handle.product()
            imdmuons += get_muon_list_out(imd_emtf_n_prod, "IMD", vhdl_dict, 4)

            emu_bar_muons = bar_handle.product()
            bar_muons = get_muon_list(emu_bar_muons, "BMTF", vhdl_dict, i)
            emu_ovl_muons = ovl_handle.product()
            ovlp_muons = get_muon_list(emu_ovl_muons, "OMTF_POS", vhdl_dict, i)
            ovln_muons = get_muon_list(emu_ovl_muons, "OMTF_NEG", vhdl_dict, i)
            emu_fwd_muons = fwd_handle.product()
            fwdp_muons = get_muon_list(emu_fwd_muons, "EMTF_POS", vhdl_dict, i)
            fwdn_muons = get_muon_list(emu_fwd_muons, "EMTF_NEG", vhdl_dict, i)

            emu_out_shower = outShower_handle.product()
            outshower = get_shower_out(emu_out_shower, vhdl_dict)
            emu_fwd_showers = fwdShowers_handle.product()
            fwdp_showers = get_shower_list(
                emu_fwd_showers, "EMTF_POS", vhdl_dict, i)
            fwdn_showers = get_shower_list(
                emu_fwd_showers, "EMTF_NEG", vhdl_dict, i)

            conversion_time = time.time() - evt_start - get_label_time

            for mu in outmuons:
                if mu.bitword != 0:
                    cntr += 1
            input_buffer.writeFrameBasedInputBX(
                bar_muons, fwdp_muons, fwdn_muons, ovlp_muons, ovln_muons, calo_sums, fwdp_showers, fwdn_showers)

            final_scoutingmuons_queue.appendleft(scoutmuons_final)
            imd_bmtf_scoutingmuons_queue.appendleft(scoutmuons_imd)
            final_scoutingshowers_queue.appendleft(outshower)
            if local_frame_cntr < vhdl_dict["SCOUTING_DELAY"]:
                final_scoutingmuons = None
                emu_imd_bmtf_scouting_muons = None
                final_scoutingshowers = None
            else:
                final_scoutingmuons = final_scoutingmuons_queue.pop()
                emu_imd_bmtf_scouting_muons = imd_bmtf_scoutingmuons_queue.pop()
                final_scoutingshowers = final_scoutingshowers_queue.pop()

            output_buffer.writeFrameBasedOutputBX(
                outmuons, imdmuons, outshower, trigger_valid=True, scouting_final_muons=final_scoutingmuons, scouting_bmtf_muons=emu_imd_bmtf_scouting_muons, scouting_final_showers=final_scoutingshowers)

            local_frame_cntr += 1

            if i % (max_events-1) == 0 and i != 0:  # dump every max_events
                drain_scoutingmuons_queue(
                    vhdl_dict, output_buffer, final_scoutingmuons_queue, imd_bmtf_scoutingmuons_queue, final_scoutingshowers_queue)
                ifile = i//max_events
                print("Writing file {pattern}_{ifile}.zip for event {i}".format(pattern=pattern, ifile=ifile, i=i))
                dump_files(path, pattern, ifile, input_buffer,
                           output_buffer, opts.delay, ALGODELAY)
                local_frame_cntr = 0

            if (i+1) % 1000 == 0:
                print("  processing the {i}th event".format(i=i+1))

            write_time = time.time() - evt_start - conversion_time
            avg_get_label_time += get_label_time
            avg_conversion_time += conversion_time
            avg_write_time += write_time
        drain_scoutingmuons_queue(
            vhdl_dict, output_buffer, final_scoutingmuons_queue, imd_bmtf_scoutingmuons_queue, final_scoutingshowers_queue)
        print("total: ", time.time() - start)
        print("setup: ", setup_time)
        print("get_label:", "avg", avg_get_label_time/float(i+1), "last", get_label_time)
        print("conversion: avg", avg_conversion_time/float(i+1), "last", conversion_time)
        print("write: avg", avg_write_time/float(i+1), "last", write_time)
        print('n final muons: ', cntr)
        if i % (max_events-1) != 0:
            ifile = i/max_events
            dump_files(path, pattern, ifile, input_buffer,
                       output_buffer, opts.delay, ALGODELAY)

        print("{} files generated.".format((i+1)//max_events))


if __name__ == "__main__":
    main()
