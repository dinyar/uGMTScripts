

from helpers.muon import Muon
from tools.vhdl import VHDLConstantsParser
from helpers.pattern_dumper import TestbenchWriter

vhdl_dict = VHDLConstantsParser.parse_vhdl_file("data/ugmt_constants.vhd")

tbw = TestbenchWriter()

muon_list = [["IN", 0x00500006208FDE0B, "in1"],
             ["IN", 0x00080E06208F200A, "in2"],
             #["IN", 0x0002F8C201FE7EC5, "in3"],
             #["IN", 0x0000300E2B34880B, "in4"],
             # ["OUT", 0x00115A4BF9E3162B, "simout1", 0x73],
             # ["OUT", 0x0005583F39E2C4AB, "simout2", 0x1F3],
             ["OUT", 0x00ABC89B3CE02DA5, "simout1", 0x7E],
             ["OUT", 0x00ABC89F3CE02D4D, "expout2", 0x7E]]

TestbenchWriter.writeMuonHeadline(tbw)
for m in muon_list:
    eta_out = 0
    add_iso = False
    if m[0] == "OUT":
        eta_out = m[3]
        add_iso = True
    mo = Muon(vhdl_dict, mu_type=m[0], bitword=m[1], eta_out=eta_out)
    TestbenchWriter.writeMuon(tbw, mo, m[2], 0, add_iso)

print("".join(tbw.string))

for m in muon_list:
    print('{0:064b}'.format(m[1]) + "   " + m[2])
