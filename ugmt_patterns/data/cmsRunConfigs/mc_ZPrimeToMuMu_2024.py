# Auto generated configuration file
# using: 
# Revision: 1.19 
# Source: /local/reps/CMSSW/CMSSW/Configuration/Applications/python/ConfigBuilder.py,v 
# with command line options: l1Ntuple -s RAW2DIGI --python_filename=mc.py -n 100 --era=Run3 --mc --conditions=133X_mcRun3_2024_realistic_v8 --customise=L1Trigger/Configuration/customiseReEmul.L1TReEmulMCFromRAW --filein=/store/mc/Run3Winter24Digi/SingleNeutrino_Pt-2To20-gun/GEN-SIM-RAW/133X_mcRun3_2024_realistic_v8-v2/2540000/038bda40-23b3-4038-a546-6397626ae3e2.root
import FWCore.ParameterSet.Config as cms

from Configuration.Eras.Era_Run3_cff import Run3

process = cms.Process('RAW2DIGI',Run3)

# import of standard configurations
process.load('Configuration.StandardSequences.Services_cff')
process.load('SimGeneral.HepPDTESSource.pythiapdt_cfi')
process.load('FWCore.MessageService.MessageLogger_cfi')
process.load('Configuration.EventContent.EventContent_cff')
process.load('SimGeneral.MixingModule.mixNoPU_cfi')
process.load('Configuration.StandardSequences.GeometryRecoDB_cff')
process.load('Configuration.StandardSequences.MagneticField_cff')
process.load('Configuration.StandardSequences.RawToDigi_cff')
process.load('Configuration.StandardSequences.EndOfProcess_cff')
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')

process.maxEvents = cms.untracked.PSet(
    input = cms.untracked.int32(-1),
    output = cms.optional.untracked.allowed(cms.int32,cms.PSet)
)

# Input source
process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring(
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/4d97de66-ce5d-4ccb-a005-69cddeae5624.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/28a756b1-43ad-47b7-9233-cc59ffa1e3bf.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/f4a51ce2-ace8-48b0-8e4a-11f3123d105d.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/5da495b0-c711-4930-97e3-5829d87375e1.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/2a43233f-2cd4-4bb0-8bb5-fe7c8c256478.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/4beaadd0-7f03-44c2-a47d-5f670b23352a.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/8472c745-6fe9-45ed-905f-f81f2c2ab112.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/e5e90bb5-7ccc-45b1-b51e-d6200ff08f21.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/7fc13b21-5653-4191-9a77-d174c9f8a018.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/446aad12-e88d-4819-a7e6-47ea84f53cfd.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/6bc8fb29-356c-4e93-9067-51d0f8b297c7.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/84d3b827-e131-44b1-a3b4-8b7d29959b8f.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/64c27bec-1465-450c-8357-c4fb8b5aed72.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/5c4850f7-3377-467c-a168-68e8e4eeb28c.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/bc842205-559b-486b-abb1-1be817657a71.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/14ae802e-9af4-4131-b4d4-e86f8f904510.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/3086e1bf-895c-40c3-8a78-fa999c47bb64.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/ffe6ad3e-c4ef-4545-8aac-b9f7baeb6c03.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/d41efe8e-7c5a-475b-89d9-f04fb204f60e.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/6c54fa9c-6a81-4abe-a19f-7b80f530a0c6.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/28653a59-72c1-4bb3-abc9-8c89fea3b6fa.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/6749e23e-26f4-4bdd-a739-4f942abd6b30.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/8038cf9f-6f35-4b84-b06d-bdf2f923b0bb.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/dbfe9c61-0d7f-4066-af4e-ddac506715ea.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/73311f36-b07c-4ad2-a3cf-a5070f3ffdde.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/bf71e5ca-f1f2-4420-875f-3b934c91d43f.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/ee00585a-c80b-4ea5-8edc-e3d5ca71bea7.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/b040e424-0bed-4982-adb9-13ebb52a5731.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/eaa48582-ac75-4d96-b84f-d26da186715c.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/55581df7-4e46-4808-83d6-4b38e8e9c73a.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/56985e91-d3b8-408d-94f3-2d3f441553dc.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/8718e26f-85b2-4fa9-b3c0-fa3de42154c1.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/4b7de6da-738e-4f2a-af11-b43383daa7ff.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/5293e028-a157-450b-b946-2e6edc57cfa6.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/2af3dfbf-dc37-487b-a6c5-5dec2955b6c4.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/1510205e-9182-4cbd-9049-67dae3a537f6.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/bee2c7b2-661b-4174-866c-e49d5c846cb1.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/37130c8e-fde0-485e-aeea-768b5084cd95.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/9566ad6b-7009-41ed-86a4-4c241a457c2f.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/7403509c-0f0f-49c9-be55-49b61fe1ac97.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/96c277b1-5239-4d77-ab03-733aa7206808.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/9dba2b0d-8ac9-415c-95dd-180e1f27e6c3.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/00aaa766-d888-473c-ada0-df4b3af5030c.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/91103f5d-e310-4868-bbed-d3a0728a7413.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/acd33402-a149-4bb6-9230-35361290ff7c.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/1e8bbbea-7940-4e51-8dfa-97aba69b715e.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/84f2bcd9-c131-4e52-acf9-d88620aa4549.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/0edd18fc-2001-4012-bff0-3da4fdadcb95.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/9982b111-73e7-4e38-9487-e7db643861e6.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/2ac18118-a1d4-4fc5-8b7c-0fe39c6fb8b8.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/db03256f-8474-40ae-aa94-26705cdc77e1.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/e260883b-44a1-4af6-b2e5-223d292ce4e8.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/d9d8f40f-5f87-473d-b052-da128b0c0c81.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/7baa68e6-4922-45e1-8016-54a6a7a5974e.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/93b7ffe6-b51a-4fa4-a170-53a692480464.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/e81026a8-3077-4e3e-a0c2-d643ec48094b.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/09018f14-db3c-4771-8a5a-97aa99224e8d.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/6a6a1979-7752-4fb9-86c4-275648649495.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/685b9119-31ee-40f5-acdd-b9ae064e0e7f.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/5cc43140-12ad-473f-aeb6-195ff5b481f6.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/70b0a54f-fd5c-4d45-a54c-baea2d6ab324.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/2550000/16d69c08-b441-4e01-8daa-5c03e124d3b9.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/810000/69b39003-6c67-412e-93dd-12d8570630e1.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/810000/b584609b-dc91-4ba8-b097-e5264dadf8f6.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/810000/b495ece6-7060-4cd5-986a-637954881822.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/810000/027616fa-d6c8-448e-ae96-e13998f2e946.root',
'/store/mc/Run3Summer23BPixDRPremix/ZprimeToMuMu_M-6000_TuneCP5_13p6TeV-pythia8/GEN-SIM-RAW/130X_mcRun3_2023_realistic_postBPix_v2-v2/810000/7ee272fc-eb95-40e4-b056-b8b33b022314.root',
),
    secondaryFileNames = cms.untracked.vstring()
)

process.load('L1Trigger.L1TMuon.hackConditions_cff')

process.options = cms.untracked.PSet(
    IgnoreCompletely = cms.untracked.vstring(),
    Rethrow = cms.untracked.vstring(),
    TryToContinue = cms.untracked.vstring(),
    accelerators = cms.untracked.vstring('*'),
    allowUnscheduled = cms.obsolete.untracked.bool,
    canDeleteEarly = cms.untracked.vstring(),
    deleteNonConsumedUnscheduledModules = cms.untracked.bool(True),
    dumpOptions = cms.untracked.bool(False),
    emptyRunLumiMode = cms.obsolete.untracked.string,
    eventSetup = cms.untracked.PSet(
        forceNumberOfConcurrentIOVs = cms.untracked.PSet(
            allowAnyLabel_=cms.required.untracked.uint32
        ),
        numberOfConcurrentIOVs = cms.untracked.uint32(0)
    ),
    fileMode = cms.untracked.string('FULLMERGE'),
    forceEventSetupCacheClearOnNewRun = cms.untracked.bool(False),
    holdsReferencesToDeleteEarly = cms.untracked.VPSet(),
    makeTriggerResults = cms.obsolete.untracked.bool,
    modulesToCallForTryToContinue = cms.untracked.vstring(),
    modulesToIgnoreForDeleteEarly = cms.untracked.vstring(),
    numberOfConcurrentLuminosityBlocks = cms.untracked.uint32(0),
    numberOfConcurrentRuns = cms.untracked.uint32(1),
    numberOfStreams = cms.untracked.uint32(0),
    numberOfThreads = cms.untracked.uint32(1),
    printDependencies = cms.untracked.bool(False),
    sizeOfStackForThreadsInKB = cms.optional.untracked.uint32,
    throwIfIllegalParameter = cms.untracked.bool(True),
    wantSummary = cms.untracked.bool(False)
)

# Production Info
process.configurationMetadata = cms.untracked.PSet(
    annotation = cms.untracked.string('l1Ntuple nevts:100'),
    name = cms.untracked.string('Applications'),
    version = cms.untracked.string('$Revision: 1.19 $')
)

# Output definition
process.FEVTDEBUGoutput = cms.OutputModule("PoolOutputModule",
    dataset = cms.untracked.PSet(
        dataTier = cms.untracked.string(''),
        filterName = cms.untracked.string('')
    ),
    fileName = cms.untracked.string('ZprimeToMuMu_M-6000_TuneCP5_13p6TeV_pythia8.root'),
    outputCommands = cms.untracked.vstring(
       'drop *',
       'keep *_simGmtShowerDigis_*_*',
       'keep *_simEmtfShowers_*_*',
       'keep *_simBmtfDigis_*_*',
       'keep *_simKBmtfDigis_*_*',
       'keep *_simOmtfDigis_*_*',
       'keep *_simEmtfDigis_*_*',
       'keep *_simGmtStage2Digis_*_*'
    ),
    splitLevel = cms.untracked.int32(0)
)

# Additional output definition

# Other statements
from Configuration.AlCa.GlobalTag import GlobalTag
process.GlobalTag = GlobalTag(process.GlobalTag, '133X_mcRun3_2024_realistic_v8', '')

# Path and EndPath definitions
process.raw2digi_step = cms.Path(process.RawToDigi)
process.endjob_step = cms.EndPath(process.endOfProcess)
process.RECOSIMoutput_step = cms.EndPath(process.FEVTDEBUGoutput)

# Schedule definition
process.schedule = cms.Schedule(process.raw2digi_step,process.endjob_step,process.RECOSIMoutput_step)
from PhysicsTools.PatAlgos.tools.helpers import associatePatAlgosToolsTask
associatePatAlgosToolsTask(process)

# customisation of the process.

# Automatic addition of the customisation function from L1Trigger.Configuration.customiseReEmul
from L1Trigger.Configuration.customiseReEmul import L1TReEmulMCFromRAW 

#call to customisation function L1TReEmulMCFromRAW imported from L1Trigger.Configuration.customiseReEmul
process = L1TReEmulMCFromRAW(process)

# End of customisation functions


# Customisation from command line

# Add early deletion of temporary data products to reduce peak memory need
from Configuration.StandardSequences.earlyDeleteSettings_cff import customiseEarlyDelete
process = customiseEarlyDelete(process)
# End adding early deletion
