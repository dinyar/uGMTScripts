# Auto generated configuration file
# using: 
# Revision: 1.19 
# Source: /local/reps/CMSSW/CMSSW/Configuration/Applications/python/ConfigBuilder.py,v 
# with command line options: l1Ntuple -s RAW2DIGI --python_filename=mc.py -n 100 --era=Run3 --mc --conditions=133X_mcRun3_2024_realistic_v8 --customise=L1Trigger/Configuration/customiseReEmul.L1TReEmulMCFromRAW --filein=/store/mc/Run3Winter24Digi/SingleNeutrino_Pt-2To20-gun/GEN-SIM-RAW/133X_mcRun3_2024_realistic_v8-v2/2540000/038bda40-23b3-4038-a546-6397626ae3e2.root
import FWCore.ParameterSet.Config as cms

from Configuration.Eras.Era_Run3_cff import Run3

process = cms.Process('RAW2DIGI',Run3)

# import of standard configurations
process.load('Configuration.StandardSequences.Services_cff')
process.load('SimGeneral.HepPDTESSource.pythiapdt_cfi')
process.load('FWCore.MessageService.MessageLogger_cfi')
process.load('Configuration.EventContent.EventContent_cff')
process.load('SimGeneral.MixingModule.mixNoPU_cfi')
process.load('Configuration.StandardSequences.GeometryRecoDB_cff')
process.load('Configuration.StandardSequences.MagneticField_cff')
process.load('Configuration.StandardSequences.RawToDigi_cff')
process.load('Configuration.StandardSequences.EndOfProcess_cff')
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')

process.maxEvents = cms.untracked.PSet(
    input = cms.untracked.int32(200000),
    output = cms.optional.untracked.allowed(cms.int32,cms.PSet)
)

# Input source
process.source = cms.Source("PoolSource",
    skipBadFiles = cms.untracked.bool(True),
    fileNames = cms.untracked.vstring(
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/52f3a7a8-ce4b-4493-ace4-23a68b10b765.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/c49d9077-afa9-4037-9f79-e346b0808a14.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/4ad01ede-ecc9-4203-a54e-bdb950f3d134.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/de2befb0-136f-4932-b590-38a2b9ec477e.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/33bc6905-15aa-43eb-bc33-78ba2416af7a.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/33cdd4a3-2f37-4680-a9aa-30a93e193267.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/87132799-364f-4301-9412-da5ce621dc39.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/2ac5b6b5-c333-4726-ad1c-265408f108a9.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/9931f85b-6e0e-4280-ba22-4f0f4c775a1e.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/381019e6-9912-461f-95f1-45124d71b469.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/17ed13c0-8b6c-44a1-badd-1441cb89e828.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/a9a8d71f-680e-468c-bc77-51d27878b15f.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/738f2c8f-e504-44c7-94c3-f329da88e3f6.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/fcdfa862-745a-46e6-9bfe-69c54f70cd7c.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/539ccae5-bcef-4bce-9533-def1e2fcf8d1.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/5e933a9c-10bc-4e01-a7ae-043f6bece2c2.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/c9e5ae9b-d9ed-4b58-87b2-fc4d982a839d.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/850c3d4c-0d71-4372-b468-df42836e0bea.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/c423211f-2f87-4cc2-8d56-1b4b9d7e8ca7.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/a63eb687-bb64-4864-82e0-10aef568771d.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/8155f2a8-ba6f-418a-8600-0cef8c9abbd7.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/6a83f40b-4e01-41f4-b987-3488bb679848.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/7e297bfe-03bb-4fe5-8e08-d51575c8d793.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/ea044ebe-7ff6-4aea-87a5-7e6f57023df4.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/c24c1a4e-4f00-4371-b178-44eca0eedcb3.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/09948b6a-8f89-4d26-8c3a-dc374ff83a6a.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/2560000/6838c73d-4415-459e-b950-6312245e89c8.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/2560000/0da7661b-d20e-4d33-a1c0-4df836737f19.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/2560000/b6f44280-3c43-49d4-bd88-d02aa40c4165.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/2560000/971cfb2e-3264-4b04-b12b-f8864f2b85b5.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/2560000/2c1e04df-8482-4f1f-8d37-408440bf7b87.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/2560000/fd39f35b-4a4e-4253-93a6-d8b53b872860.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/2560000/ee63b2a7-607c-4dd2-acc0-26fd52709a04.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/2560000/385a5e06-6c28-40fd-be18-c62e01d434f3.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/2560000/e2c64b89-7c26-4d5d-b6c9-034bef9c8272.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/c40d29ab-3777-47e6-b9fb-9e3e9ac357fe.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/cc769af4-170b-49c2-9498-d9fbac17a65b.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/1881ce57-c50f-4443-8bc9-3f1ce557ec7f.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/79644820-cf42-4a81-916c-b69e8eb4ae47.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/bf5285d4-51fe-4330-a8ab-1f5a21f20e52.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/312d1638-2c8a-401e-b80d-11ff35f783c1.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/3b69e6e4-e197-4f05-b90b-aa6df2dafe89.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/601fab99-82e4-4138-833a-fd74edf9c058.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/3ad0297c-ab44-4584-9a0a-ffe28b2ce2ab.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/376bb217-71a2-4db8-8f1d-e51eb36df144.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/f936fc67-eea5-4159-88ca-bff36908639d.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/78c3ae54-8278-4be4-aebd-de963e4bce41.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/96e7fb4f-a8b0-4349-b054-e42ad91eefbb.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/09871532-2ba8-445d-b9dd-4f3c8c7b1996.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/fbe33942-2d3f-4f0a-97e1-132ebfbc9fed.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/404cfd31-a457-404c-9b6b-f92f92ac5feb.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/13564af7-5965-4825-829d-ae0ecc06a843.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/613cdafa-7c20-4bc0-be38-ce0ee820739e.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/105cccaa-5159-4d26-977d-0a8c77425505.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/ed6ae556-9a5b-4e12-85d4-15b18adfa6d6.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/997d902c-79fa-476f-bbc5-f368ef574789.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/7af90dbf-8549-4c12-8cfc-828c56bad01c.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/00df1d29-5ba3-4fbb-a0bb-181ceddfa225.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/d54a0112-1d85-48f0-9bcb-935d2869cb9c.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/a146c9c4-8fad-40cc-b229-1e398a8f6791.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/ba5067ef-be5a-4474-9f95-8af92bff9787.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/2560000/8359b714-7c22-4ae7-a8d1-3063d3316ae9.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/2560000/052cb98b-0fd4-4a2f-a7c0-d9f954389d61.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/2560000/c19fd7f0-ee9a-4efa-855c-18295033c795.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/2560000/4edd66ea-d81f-45c5-be62-1a541f0bf2f6.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/2560000/3176099e-ab34-4513-9953-eb9574c5039c.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/2560000/e90fe7f8-854e-4e3d-a915-25137d93a66b.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/2560000/a3047993-d5ff-494c-80ea-f64233487e32.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/2560000/43b4af38-cdc1-4ab9-941b-c3292c44ffa3.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/2560000/c36adedc-f396-459e-86a0-dafbdee4d33b.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/2560000/3d6da3c4-8365-43dc-9990-4bdee2a9fb38.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/2560000/e0cbf252-142c-4286-92b7-a5563c66351c.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/2560000/c20373f1-1d7e-4f52-834e-405a93f44a92.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/2560000/704e2524-b208-4ab3-bd94-484502e88876.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/2560000/a8ffc749-2cdc-43f7-b0ac-70bae1375f3a.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/2560000/b2e49fd6-6719-4e0a-b7a6-98df606d679f.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/2560000/cece9e99-5240-433e-8586-140758b845a0.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/2560000/348b307b-0b0a-4683-a9bb-371910167e8f.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/2560000/6bbad434-b812-464b-ac7a-fa3d69c4537a.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/2560000/4ef03d36-434c-43b1-b9f0-c965dbdca0e9.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/2560000/4e7d5f7c-cd5f-4d52-8269-54e3281115e5.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/71217d65-80e2-4220-b6b8-e8cf6a5448ef.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/84723446-0e9a-444f-9eb0-a20aa07190c2.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/98734cf4-5144-467b-b301-f864487d16ad.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/eb8a8142-da04-4d1d-bb23-6e7e6f9fdec8.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/4d119769-1e16-4d1c-b37d-7bc13c47dc5c.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/2e5db335-c3ec-4922-8d24-826736ae76d9.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/bc8c5c75-286c-4a1c-882b-441460fb7493.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/3e87e8b8-9409-4c6d-b7a1-c5f0ae192220.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/6b4e6cbc-2376-487c-bd2c-d5d47eb63fbe.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/ceb1c114-a184-4403-8abe-ae42b4d9dc51.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/b5d09b79-5849-42ff-a771-91d483918f03.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/81102f03-f955-42bb-b31b-b14786811fc1.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/4acf3fe2-8fe6-4dc2-b619-d8544342ccf9.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/34a64184-9de7-4719-9b22-2b212ae4d350.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/f9d018bf-981c-472a-8f98-096b8c595cf3.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/420dd392-45da-4103-9000-16c2eae94366.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/f066824c-2eb5-405e-8be3-1431e00ef6f3.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/6d9b49a7-c390-41f4-ac6c-f239c67a8e66.root',
'/store/mc/Run3Summer23BPixDRPremix/JPsiToMuMu_PT-0to100_pythia8-gun/GEN-SIM-RAW/KeepSi_130X_mcRun3_2023_realistic_postBPix_v2-v2/50000/4d742ece-e3ba-4cb7-bdc3-7f889ca12470.root',
),
    secondaryFileNames = cms.untracked.vstring()
)

process.load('L1Trigger.L1TMuon.hackConditions_cff')

process.options = cms.untracked.PSet(
    IgnoreCompletely = cms.untracked.vstring(),
    Rethrow = cms.untracked.vstring(),
    TryToContinue = cms.untracked.vstring(),
    accelerators = cms.untracked.vstring('*'),
    allowUnscheduled = cms.obsolete.untracked.bool,
    canDeleteEarly = cms.untracked.vstring(),
    deleteNonConsumedUnscheduledModules = cms.untracked.bool(True),
    dumpOptions = cms.untracked.bool(False),
    emptyRunLumiMode = cms.obsolete.untracked.string,
    eventSetup = cms.untracked.PSet(
        forceNumberOfConcurrentIOVs = cms.untracked.PSet(
            allowAnyLabel_=cms.required.untracked.uint32
        ),
        numberOfConcurrentIOVs = cms.untracked.uint32(0)
    ),
    fileMode = cms.untracked.string('FULLMERGE'),
    forceEventSetupCacheClearOnNewRun = cms.untracked.bool(False),
    holdsReferencesToDeleteEarly = cms.untracked.VPSet(),
    makeTriggerResults = cms.obsolete.untracked.bool,
    modulesToCallForTryToContinue = cms.untracked.vstring(),
    modulesToIgnoreForDeleteEarly = cms.untracked.vstring(),
    numberOfConcurrentLuminosityBlocks = cms.untracked.uint32(0),
    numberOfConcurrentRuns = cms.untracked.uint32(1),
    numberOfStreams = cms.untracked.uint32(0),
    numberOfThreads = cms.untracked.uint32(1),
    printDependencies = cms.untracked.bool(False),
    sizeOfStackForThreadsInKB = cms.optional.untracked.uint32,
    throwIfIllegalParameter = cms.untracked.bool(True),
    wantSummary = cms.untracked.bool(False)
)

# Production Info
process.configurationMetadata = cms.untracked.PSet(
    annotation = cms.untracked.string('l1Ntuple nevts:100'),
    name = cms.untracked.string('Applications'),
    version = cms.untracked.string('$Revision: 1.19 $')
)

# Output definition
process.FEVTDEBUGoutput = cms.OutputModule("PoolOutputModule",
    dataset = cms.untracked.PSet(
        dataTier = cms.untracked.string(''),
        filterName = cms.untracked.string('')
    ),
    fileName = cms.untracked.string('JPsiToMuMu_PT-0to100_pythia8-gun.root'),
    outputCommands = cms.untracked.vstring(
       'drop *',
       'keep *_simGmtShowerDigis_*_*',
       'keep *_simEmtfShowers_*_*',
       'keep *_simBmtfDigis_*_*',
       'keep *_simKBmtfDigis_*_*',
       'keep *_simOmtfDigis_*_*',
       'keep *_simEmtfDigis_*_*',
       'keep *_simGmtStage2Digis_*_*'
    ),
    splitLevel = cms.untracked.int32(0)
)

# Additional output definition

# Other statements
from Configuration.AlCa.GlobalTag import GlobalTag
process.GlobalTag = GlobalTag(process.GlobalTag, '133X_mcRun3_2024_realistic_v8', '')

# Path and EndPath definitions
process.raw2digi_step = cms.Path(process.RawToDigi)
process.endjob_step = cms.EndPath(process.endOfProcess)
process.RECOSIMoutput_step = cms.EndPath(process.FEVTDEBUGoutput)

# Schedule definition
process.schedule = cms.Schedule(process.raw2digi_step,process.endjob_step,process.RECOSIMoutput_step)
from PhysicsTools.PatAlgos.tools.helpers import associatePatAlgosToolsTask
associatePatAlgosToolsTask(process)

# customisation of the process.

# Automatic addition of the customisation function from L1Trigger.Configuration.customiseReEmul
from L1Trigger.Configuration.customiseReEmul import L1TReEmulMCFromRAW 

#call to customisation function L1TReEmulMCFromRAW imported from L1Trigger.Configuration.customiseReEmul
process = L1TReEmulMCFromRAW(process)

# End of customisation functions


# Customisation from command line

# Add early deletion of temporary data products to reduce peak memory need
from Configuration.StandardSequences.earlyDeleteSettings_cff import customiseEarlyDelete
process = customiseEarlyDelete(process)
# End adding early deletion
